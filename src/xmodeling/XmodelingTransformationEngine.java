package xmodeling;

import java.io.ByteArrayInputStream;import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * <b> XmodelingTransformationEngine is the class dealing with metamodel-to-metamodel transformation. </b>
 * 
 * @author LeaBrunschwig
 */
public class XmodelingTransformationEngine {
		
	/**
	 * Create the workflow metamodel elements to be added to the
	 * other metamodel after concatenation
	 * 
	 * @return ArrayList<EClass> the list of all elements from the workflow
	 * 			metamodel.
	 */
	@SuppressWarnings("unchecked")
	protected ArrayList<EClass> getWorkflowElements() {

		EcoreFactory factory = EcoreFactory.eINSTANCE;
		EcorePackage pack = EcorePackage.eINSTANCE;
		
		// classes
		EClass Operation = factory.createEClass();
		EClass Action = factory.createEClass();
		
		// references
		EReference onExitRef = factory.createEReference();
		EReference onEntryRef = factory.createEReference();
		EReference onDoRef = factory.createEReference();
		
		// attributes
		EAttribute nameOperation = factory.createEAttribute();
		EAttribute objectOperation = factory.createEAttribute();
		EAttribute parametersOperation = factory.createEAttribute();
		EAttribute returnOperation = factory.createEAttribute();
		
		// operations
		EOperation execute = factory.createEOperation();
		EOperation onExitOp = factory.createEOperation();
		EOperation onEntryOp = factory.createEOperation();
		EOperation onDoOp = factory.createEOperation();

		/******************/
		/* Xmod_Operation */
		/******************/
		Operation.setName("Xmod_Operation");

		// attribut nameOperation
		nameOperation.setName("name");
		nameOperation.setLowerBound(0);
		nameOperation.setUpperBound(1);
		nameOperation.setEType(pack.getEString());
		
		// attribut objectOperation
		objectOperation.setName("objectTag");
		objectOperation.setLowerBound(1);
		objectOperation.setUpperBound(1);
		objectOperation.setEType(pack.getEString());
		
		// attribut parametersOperation
		parametersOperation.setName("parametersTag");
		parametersOperation.setLowerBound(0);
		parametersOperation.setUpperBound(-1);
		parametersOperation.setEType(pack.getEString());
		
		// attribut returnOperation
		returnOperation.setName("returnTag");
		returnOperation.setLowerBound(0);
		returnOperation.setUpperBound(1);
		returnOperation.setEType(pack.getEString());
		
		// operation execute
		execute.setName("execute");
		execute.setLowerBound(0);
		execute.setUpperBound(1);

		Operation.getEStructuralFeatures().add(nameOperation);
		Operation.getEStructuralFeatures().add(objectOperation);
		Operation.getEStructuralFeatures().add(parametersOperation);
		Operation.getEStructuralFeatures().add(returnOperation);
		Operation.getEOperations().add(execute);

		/***************/
		/* Xmod_Action */
		/***************/
		Action.setName("Xmod_Action");

		// operation onEntryOp
		onEntryOp.setName("onEntry");
		onEntryOp.setLowerBound(0);
		onEntryOp.setUpperBound(1);

		// operation onExitOp
		onExitOp.setName("onExit");
		onExitOp.setLowerBound(0);
		onExitOp.setUpperBound(1);
		
		// operation onDoOp
		onDoOp.setName("onDo");
		onDoOp.setLowerBound(0);
		onDoOp.setUpperBound(1);
		
		// reference onEntryRef
		onEntryRef.setEType(Operation);
		onEntryRef.setLowerBound(0);
		onEntryRef.setUpperBound(1);
		onEntryRef.setContainment(true);
		onEntryRef.setName("onEntry");
		
		// reference onExitRef
		onExitRef.setEType(Operation);
		onExitRef.setLowerBound(0);
		onExitRef.setUpperBound(1);
		onExitRef.setContainment(true);
		onExitRef.setName("onExit");
		
		// reference onDoRef
		onDoRef.setEType(Operation);
		onDoRef.setLowerBound(0);
		onDoRef.setUpperBound(1);
		onDoRef.setContainment(true);
		onDoRef.setName("onDo");

		Action.getEOperations().add(onEntryOp);
		Action.getEOperations().add(onExitOp);
		Action.getEOperations().add(onDoOp);
		Action.getEStructuralFeatures().add(onEntryRef);
		Action.getEStructuralFeatures().add(onExitRef);
		Action.getEStructuralFeatures().add(onDoRef);

		ArrayList<EClass> liste = new ArrayList<EClass>();
		liste.add(Operation);
		liste.add(Action);

		return (ArrayList<EClass>) liste.clone();
	}
	/**
	 * <p>modifyMetaModel has two purposes :
	 * <ul>
	 * <li>Add to the meta-model the Xmod classes regarding the position of the Xmod_exec annotation.</li>
	 * <li>Add a method at the end of the [projectName]XmodUtil class regarding the Xmod_main annotation position.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param mmSource
	 *            file containing the original meta-model
	 * @param mmTarget
	 *            file in which the modified meta-model is saved
	 * @throws IOException
	 * @throws CoreException
	 * @see XmodelingTransformationEngine#getWorkflowElements()
	 */
	public void modifyMetaModel(String mmSource, String mmTarget) throws IOException, CoreException {

		/* Init */
		String projectClass, content, projectName = "";
    	StringBuffer sb = new StringBuffer();
    	String buf;
		EcoreFactory factory = EcoreFactory.eINSTANCE;
		URI uri;
		Factory resourceFactory;
		Resource resource;
		ArrayList<EClass> liste, listeClasses;
		TreeIterator<EObject> treeit;
		IProject p;
		IFile f;
		InputStream s;
		
		// loading of the original MM
		uri = URI.createFileURI(mmSource);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		resourceFactory = new XMIResourceFactoryImpl();
		resource = resourceFactory.createResource(uri);
		resource.load(null);

		// modify the URI to the modified meta-model
		uri = URI.createFileURI(mmTarget);
		resource.setURI(uri);

		// creation of the Workflow classes
		liste = getWorkflowElements();

		// parcours des �l�ments du meta-mod�le
		treeit = resource.getAllContents();
		listeClasses = new ArrayList<EClass>();
		while (treeit.hasNext()) {

			EObject eobject = treeit.next();

			// if we find the package of the MM, we add all the items
			// concerning the workflow
			if (eobject instanceof EPackage) {
				EPackage pack = (EPackage) eobject;
				projectName = pack.getName();
				for (int i = 0; i < liste.size(); i++) {
					pack.getEClassifiers().add(liste.get(i));
				}
			}

			// forme la liste de toutes les classes du m�ta-mod�le (en excluant
			// celles qu'on vient de rajouter)
			if (eobject instanceof EClass) {
				EClass eclass = (EClass) eobject;
				if (!liste.contains(eclass))
					listeClasses.add(eclass);
			}
		}

		for (EClass eclass : listeClasses) {
			for(int i = 0; i<eclass.getEAnnotations().size(); i++) {
				if(eclass.getEAnnotations().get(i).getSource().equals("Xmod_exec")) { // if a class owns an annotation "Xmod_exec"
					// Action extends this class
					eclass.getESuperTypes().add(liste.get(1));
					// We create a reference between this class and Operation
					EReference operation = factory.createEReference();
					// reference annotations
					operation.setEType(liste.get(0));
					operation.setLowerBound(0);
					operation.setUpperBound(-1);
					operation.setContainment(true);
					operation.setName("operation");
					eclass.getEStructuralFeatures().add(operation);
				}
				if(eclass.getEAnnotations().get(i).getSource().equals("Xmod_main")) { // if a class owns an annotation "Xmod_main"
					
					// because a class name begin with an upper-case letter we need to format it.
					buf = projectName;
			    	projectClass = buf.substring(0,1).toUpperCase();
			    	sb.append(projectClass);
			    	sb.append(buf.substring(1, buf.length()));
			    	projectClass = sb.toString();
			    	
			    	// the file <projectName>XmodUtil.java is going to be edited. 
			    	// we add the method load<rootElement>(String uri) to the class depending 
			    	// on what is the root element annotated with Xmod_main
			    	p = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			    	f = p.getFolder("src-gen/"+p.getName()+"/util").getFile(projectClass+"XmodUtil.java");
			    	if (!f.exists()) { // if the original <projectName>XmodUtil.java file has been deleted
						f.create(null, true, null);
		        	}
			    	content = "package "+projectName+".util;\r\n" + 
			    			"\r\n" + 
			    			"import java.beans.XMLDecoder;\r\n" + 
			    			"import java.beans.XMLEncoder;\r\n" + 
			    			"import java.io.BufferedInputStream;\r\n" + 
			    			"import java.io.BufferedOutputStream;\r\n" + 
			    			"import java.io.File;\r\n" + 
			    			"import java.io.FileInputStream;\r\n" + 
			    			"import java.io.FileNotFoundException;\r\n" + 
			    			"import java.io.FileOutputStream;\r\n" + 
			    			"import java.util.HashMap;\r\n" + 
			    			"\r\n" + 
			    			"import org.eclipse.emf.common.util.TreeIterator;\r\n" + 
			    			"import org.eclipse.emf.common.util.URI;\r\n" + 
			    			"import org.eclipse.emf.ecore.EObject;\r\n" + 
			    			"import org.eclipse.emf.ecore.EPackage;\r\n" + 
			    			"import org.eclipse.emf.ecore.resource.Resource;\r\n" + 
			    			"import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;\r\n" + 
			    			"import org.eclipse.emf.ecore.xmi.XMLResource;\r\n" + 
			    			"import org.eclipse.emf.ecore.xmi.XMLResource.XMLMap;\r\n" + 
			    			"import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;\r\n" + 
			    			"import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;\r\n" + 
			    			"import "+projectName+"."+eclass.getName()+";\r\n" + 
			    			"\r\n" + 
			    			"import "+projectName+"."+projectClass+"Package;\r\n" + 
			    			"\r\n" + 
			    			"\r\n" + 
			    			"/**\r\n" + 
			    			" * <b> "+projectClass+"XmodUtil is a class that provides tools to manipulate the model and the map. </b>\r\n" + 
			    			" * \r\n" + 
			    			" * @author LeaBrunschwig\r\n" + 
			    			" */" +
			    			"public class "+projectClass+"XmodUtil {\r\n" + 
			    			"	\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * The map containing all the tags.\r\n" + 
			    			"	 * \r\n" + 
			    			"	 * @see #getMap()\r\n" + 
			    			"	 * @see #setMap(HashMap)\r\n" + 
			    			"	 * @see #loadMap(String)\r\n" + 
			    			"	 * @see #saveMap(String)\r\n" + 
			    			"	 */" +
			    			"	private static HashMap<String,Object> hm;\r\n" + 
			    			"	\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Returns the value of the map.\r\n" + 
			    			"	 * <p>The singleton design pattern is used.</p>\r\n" + 
			    			"	 * @return the map hm.\r\n" + 
			    			"	 * @see #hm\r\n" + 
			    			"	 */" +
			    			"	public static HashMap<String,Object> getMap() {\r\n" + 
			    			"		if (hm == null) {\r\n" + 
			    			"			hm = new HashMap<String,Object>();\r\n" + 
			    			"		}\r\n" + 
			    			"		return hm;\r\n" + 
			    			"	}\r\n" + 
			    			"\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Updates the map hm.\r\n" + 
			    			"	 * @param hm\r\n" + 
			    			"	 * 			Map that is going to be set to the attribute hm.\r\n" + 
			    			"	 * @see #hm\r\n" + 
			    			"	 */" +
			    			"	public static void setMap(HashMap<String,Object> hm) {\r\n" + 
			    			"		"+projectClass+"XmodUtil.hm = hm;\r\n" + 
			    			"	}\r\n" + 
			    			"\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Loads a map from a file.\r\n" + 
			    			"	 * <p>The map is deserialized.</p>\r\n" + 
			    			"	 * @param fileName\r\n" + 
			    			"	 * 			Name of the file containing a map.\r\n" + 
			    			"	 * @return a map containing the map from the given file.\r\n" + 
			    			"	 */" +
			    			"	@SuppressWarnings(\"unchecked\")\r\n" + 
			    			"	public static java.util.Map<String, Object> loadMap(String fileName) {\r\n" + 
			    			"\r\n" + 
			    			"		/* Init */\r\n" + 
			    			"		File xmlFile;\r\n" + 
			    			"		XMLDecoder decoder = null;\r\n" + 
			    			"		HashMap<String, Object> hm = new HashMap<String, Object>();\r\n" + 
			    			"\r\n" + 
			    			"		/* Body */\r\n" + 
			    			"		try {\r\n" + 
			    			"			xmlFile = new File(fileName);\r\n" + 
			    			"			if (xmlFile.exists()) { // if an xml file with the name of the map does exist, we deserialize it.\r\n" + 
			    			"				decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(xmlFile)));\r\n" + 
			    			"				hm = (HashMap<String, Object>) decoder.readObject();\r\n" + 
			    			"				decoder.close();\r\n" + 
			    			"			}\r\n" + 
			    			"\r\n" + 
			    			"		} catch (FileNotFoundException e1) {\r\n" + 
			    			"			e1.printStackTrace();\r\n" + 
			    			"		}\r\n" + 
			    			"\r\n" + 
			    			"		return hm;\r\n" + 
			    			"	}\r\n" + 
			    			"	\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Saves a map in a file.\r\n" + 
			    			"	 * <p>The map is serialized.</p>\r\n" + 
			    			"	 * @param fileName\r\n" + 
			    			"	 * 			Name of the file where the map is going to be saved.\r\n" + 
			    			"	 */" +
			    			"	public static void saveMap(String fileName) {\r\n" + 
			    			"\r\n" + 
			    			"		/* Init */\r\n" + 
			    			"		File xmlFile;\r\n" + 
			    			"		XMLEncoder encoder = null;\r\n" + 
			    			"\r\n" + 
			    			"		/* Body */\r\n" + 
			    			"		try {\r\n" + 
			    			"			xmlFile = new File(fileName);\r\n" + 
			    			"			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(xmlFile)));\r\n" + 
			    			"			encoder.writeObject(getMap());\r\n" + 
			    			"			encoder.flush();\r\n" + 
			    			"		} catch (FileNotFoundException e) {\r\n" + 
			    			"			e.printStackTrace();\r\n" + 
			    			"		} finally {\r\n" + 
			    			"			// closing of the encoder\r\n" + 
			    			"			encoder.close();\r\n" + 
			    			"		}\r\n" + 
			    			"	}\r\n" + 
			    			"	\r\n" + 
			    			"\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Saves a given model at a given URI.\r\n" + 
			    			"	 * @param uri\r\n" + 
			    			"	 * 			The URI where the model will be saved.\r\n" + 
			    			"	 * @param root\r\n" + 
			    			"	 * 			The root of the model that is going to be saved.\r\n" + 
			    			"	 */" +
			    			"	public static void saveModel(String uri, EObject root) {\r\n" + 
			    			"		Resource resource = null;\r\n" + 
			    			"		try {\r\n" + 
			    			"			URI uriUri = URI.createURI(uri);\r\n" + 
			    			"			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(\"xmi\", new XMIResourceFactoryImpl());\r\n" + 
			    			"			resource = (new ResourceSetImpl()).createResource(uriUri);\r\n" + 
			    			"			resource.getContents().add(root);\r\n" + 
			    			"			resource.save(null); \r\n" + 
			    			"		} catch (Exception e) { \r\n" + 
			    			"			System.err.println(\"Error in saving the model: \"+e);\r\n" + 
			    			"			e.printStackTrace(); \r\n" + 
			    			"		} \r\n" + 
			    			"	}\r\n" + 
			    			"\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Load a model from a given URI.\r\n" + 
			    			"	 * @param uri\r\n" + 
			    			"	 * 			The URI where the model is.\r\n" + 
			    			"	 * @param pack\r\n" + 
			    			"	 * 			an instance of EPackage\r\n" + 
			    			"	 * @return a model\r\n" + 
			    			"	 */" +
			    			"	public static Resource loadModel(String uri, EPackage pack) {\r\n" + 
			    			"		Resource resource = null;\r\n" + 
			    			"		try {\r\n" + 
			    			"			URI uriUri = URI.createURI(uri);\r\n" + 
			    			"			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(\"xmi\", new XMIResourceFactoryImpl());\r\n" + 
			    			"			resource = (new ResourceSetImpl()).createResource(uriUri);\r\n" + 
			    			"			XMLResource.XMLMap xmlMap = new XMLMapImpl();\r\n" + 
			    			"			xmlMap.setNoNamespacePackage(pack);\r\n" + 
			    			"			java.util.Map<String, XMLMap> options = new java.util.HashMap<String, XMLMap>();\r\n" + 
			    			"			options.put(XMLResource.OPTION_XML_MAP, xmlMap);\r\n" + 
			    			"			resource.load(options);    \r\n" + 
			    			"		}\r\n" + 
			    			"		catch(Exception e) {\r\n" + 
			    			"			System.err.println(\"Error while loading the model: \"+e);\r\n" + 
			    			"			e.printStackTrace();\r\n" + 
			    			"		}\r\n" + 
			    			"		return resource;\r\n" + 
			    			"	}\r\n" + 
			    			"	\r\n" + 
			    			"	/**\r\n" + 
			    			"	 * Load the "+eclass.getName()+" of the model, in other word, the root of the model.\r\n" + 
			    			"	 * @param uri\r\n" + 
			    			"	 * 			The location of the model.\r\n" + 
			    			"	 * @return the root of the model: in our case the "+eclass.getName()+".\r\n" + 
			    			"	 * @see #loadModel(String, EPackage)\r\n" + 
			    			"	 * @see "+eclass.getName()+"\r\n" + 
			    			"	 */" +
			    			"	public static "+eclass.getName()+" load"+eclass.getName()+"(String uri) {\r\n" + 
			    			"		Resource resource = "+projectName+".util."+projectClass+"XmodUtil.loadModel(uri, "+projectClass+"Package.eINSTANCE);\r\n" + 
			    			"		if (resource == null) return null;\r\n" + 
			    			"		\r\n" + 
			    			"		TreeIterator<EObject> it = resource.getAllContents();\r\n" + 
			    			"		\r\n" + 
			    			"		"+eclass.getName()+" proc = null;\r\n" + 
			    			"		while(it.hasNext()) {\r\n" + 
			    			"			EObject obj = it.next();\r\n" + 
			    			"			if (obj instanceof "+eclass.getName()+") { \r\n" + 
			    			"				proc = ("+eclass.getName()+")obj;	\r\n" + 
			    			"				break;	\r\n" + 
			    			"			}\r\n" + 
			    			"		}\r\n" + 
			    			"		if (proc == null) {\r\n" + 
			    			"			System.err.println(\"No root instance in the model\");\r\n" + 
			    			"		}\r\n" + 
			    			"		return proc;\r\n" + 
			    			"	}\r\n" + 
			    			"}\r\n" + 
			    			"";
			    	s = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
					f.setContents(s, true, true, null);
				}
			}
		}

		// saving of the modified MM
		try {
			resource.save(null);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null,
					"Impossible to save the modified model", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
