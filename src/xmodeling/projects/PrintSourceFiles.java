package xmodeling.projects;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

/**
 * <b> PrintSourceFiles is the class responsible to generate new files with content. </b>
 * <p>
 * A file may need the following informations:
 * <ul>
 * <li>the name of the project owning the file,</li>
 * <li>the name of the project but this time with an uppercase for the first letter,</li>
 * <li>the referenced project.</li>
 * </ul>
 * 
 * @author LeaBrunschwig
 *
 */
public class PrintSourceFiles {
	
	/**
	 * @see PrintSourceFiles#PrintSourceFiles(IProject, String, String)
	 */
	private String _projectName;
	/**
	 * @see PrintSourceFiles#PrintSourceFiles(IProject, String, String)
	 */
	private String _projectClass;
	/**
	 * @see PrintSourceFiles#PrintSourceFiles(IProject, String, String)
	 */
	private IProject _project;
	
	/**
	 * Constructor PrintSourceFiles.
	 * 
	 * @param project
	 * 			The referenced project.
	 * @param projectName
	 * 			The name of the project owing the file.
	 * @param projectClass
	 * 			The name of the project but with an uppercase for the first letter.
	 * 
	 * @see PrintSourceFiles#_project
	 * @see PrintSourceFiles#_projectClass
	 * @see PrintSourceFiles#_projectName
	 */
	protected PrintSourceFiles(IProject project, String projectName, String projectClass) {
		_project = project;
		_projectName = projectName;
		_projectClass = projectClass;
	}

	/**
	 * Create the file in the specified folder with the specified name and the corresponding content.
	 * <p>
	 * Authorized file name:
	 * <ul>
	 * <li>build.properties</li>
	 * <li>plugin.properties</li>
	 * <li>plugin.xml</li>
	 * <li>.project</li>
	 * <li>.classpath</li>
	 * <li>[projectName].aird</li>
	 * <li>[projectName].ecore</li>
	 * <li>[projectName].genmodel</li>
	 * <li>MANIFEST.MF</li>
	 * <li>Xmod_ActionImpl.java</li>
	 * <li>Xmod_Action.java</li>
	 * <li>Xmod_OperationImpl.java</li>
	 * <li>Xmod_Operation.java</li>
	 * <li>[projectClass]PackageImpl.java</li>
	 * <li>[projectClass]Package.java</li>
	 * <li>[projectClass]FactoryImpl.java</li>
	 * <li>[projectClass]Factory.java</li>
	 * <li>[projectClass]AdapterFactory.java</li>
	 * <li>[projectClass]Switch.java</li>
	 * <li>[projectClass]XmodUtil.java</li>
	 * </ul>
	 * </p>
	 * 
	 * @param folder
	 * 			The folder where the file will be created.
	 * @param file
	 * 			The name of the file, the corresponding content will be printed.
	 * 
	 * @see PrintSourceFiles#switchForContentFile(String)
	 * @see PrintSourceFiles#_project
	 */
	protected void createFile(String folder, String file) {
		
		/* Init */
		IFile f;
		String content;
		InputStream s;
		
		/* Body */
		if (folder != null) {
			f = _project.getFolder(folder).getFile(file);
		} else {
			f = _project.getFile(file);
		}
		try {
        	if (!f.exists()) {
				f.create(null, true, null);
        	}
        	// this method gather the content of the file
	        content = switchForContentFile(file);
	        if(content == null) { // because some file names are not final, we cannot use them inside the switch
	        	if(file.contains("aird"))
	        		content = switchForContentFile("aird");
	        	if(file.contains("ecore"))
	        		content = switchForContentFile("ecore");
	        	if(file.contains("genmodel"))
	        		content = switchForContentFile("genmodel");
	        	if(file.contains("Factory.java"))
	        		content = switchForContentFile("Factory.java");
	        	if(file.contains("FactoryImpl.java"))
	        		content = switchForContentFile("FactoryImpl.java");
	        	if(file.contains("Switch.java"))
	        		content = switchForContentFile("Switch.java");
	        	if(file.contains("AdapterFactory.java"))
	        		content = switchForContentFile("AdapterFactory.java");
	        	if(file.contains("PackageImpl.java"))
	        		content = switchForContentFile("PackageImpl.java");
	        	if(file.contains("Package.java"))
	        		content = switchForContentFile("Package.java");
	        	if(file.contains("XmodUtil.java"))
	        		content = switchForContentFile("XmodUtil.java");
	        }
	        s = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
			f.setContents(s, true, true, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * switchForContentFile returns the content of a specific file through a switch.
	 * <p>
	 * The switch is composed of the following cases:
	 * <ul>
	 * <li>build.properties</li>
	 * <li>plugin.properties</li>
	 * <li>plugin.xml</li>
	 * <li>.project</li>
	 * <li>.classpath</li>
	 * <li>aird</li>
	 * <li>ecore</li>
	 * <li>genmodel</li>
	 * <li>MANIFEST.MF</li>
	 * <li>Xmod_ActionImpl.java</li>
	 * <li>Xmod_Action.java</li>
	 * <li>Xmod_OperationImpl.java</li>
	 * <li>Xmod_Operation.java</li>
	 * <li>PackageImpl.java</li>
	 * <li>Package.java</li>
	 * <li>FactoryImpl.java</li>
	 * <li>Factory.java</li>
	 * <li>AdapterFactory.java</li>
	 * <li>Switch.java</li>
	 * <li>XmodUtil.java</li>
	 * </ul>
	 * </p>
	 * 
	 * @param id
	 * 			Correspond to one of the cases enumerated previously.
	 * 
	 * @return a string containing the corresponding content for a file
	 * 
	 * @see PrintSourceFiles#_projectClass
	 * @see PrintSourceFiles#_projectName
	 */
	private String switchForContentFile(String id) {
		
		String returnValue = null;
		
		switch (id) {
        case "build.properties":  returnValue = "#\r\n" + 
        		"\r\n" + 
        		"bin.includes = .,\\\r\n" + 
        		"               model/,\\\r\n" + 
        		"               META-INF/,\\\r\n" + 
        		"               plugin.properties,\\\r\n" + 
        		"               plugin.xml\r\n" + 
        		"jars.compile.order = .\r\n" + 
        		"source.. = src-gen/,\\\r\n" + 
        		"           src/\r\n" + 
        		"output.. = bin/\r\n" + 
        		"";
                break;
        case "plugin.properties":  returnValue = "#\r\n" + 
        		"\r\n" + 
        		"pluginName = "+_projectClass+" Model\r\n" + 
        		"providerName = www.example.org\r\n" + 
        		"";
                break;
        case "plugin.xml":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<?eclipse version=\"3.0\"?>\r\n" + 
        		"\r\n" + 
        		"<!--\r\n" + 
        		"-->\r\n" + 
        		"\r\n" + 
        		"<plugin>\r\n" + 
        		"\r\n" + 
        		"   <extension point=\"org.eclipse.emf.ecore.generated_package\">\r\n" + 
        		"      <!-- @generated "+_projectName+" -->\r\n" + 
        		"      <package\r\n" + 
        		"            uri=\"http://www.example.org/"+_projectName+"\"\r\n" + 
        		"            class=\""+_projectName+"."+_projectClass+"Package\"\r\n" + 
        		"            genModel=\"model/"+_projectName+".genmodel\"/>\r\n" + 
        		"   </extension>\r\n" + 
        		"\r\n" + 
        		"</plugin>\r\n" + 
        		"";
                break;
        case ".project":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<projectDescription>\r\n" + 
        		"	<name>"+_projectName+"</name>\r\n" + 
        		"	<comment></comment>\r\n" + 
        		"	<projects>\r\n" + 
        		"	</projects>\r\n" + 
        		"	<buildSpec>\r\n" + 
        		"		<buildCommand>\r\n" + 
        		"			<name>org.eclipse.xtext.ui.shared.xtextBuilder</name>\r\n" + 
        		"			<arguments>\r\n" + 
        		"			</arguments>\r\n" + 
        		"		</buildCommand>\r\n" + 
        		"		<buildCommand>\r\n" + 
        		"			<name>org.eclipse.jdt.core.javabuilder</name>\r\n" + 
        		"			<arguments>\r\n" + 
        		"			</arguments>\r\n" + 
        		"		</buildCommand>\r\n" + 
        		"		<buildCommand>\r\n" + 
        		"			<name>org.eclipse.pde.ManifestBuilder</name>\r\n" + 
        		"			<arguments>\r\n" + 
        		"			</arguments>\r\n" + 
        		"		</buildCommand>\r\n" + 
        		"		<buildCommand>\r\n" + 
        		"			<name>org.eclipse.pde.SchemaBuilder</name>\r\n" + 
        		"			<arguments>\r\n" + 
        		"			</arguments>\r\n" + 
        		"		</buildCommand>\r\n" + 
        		"	</buildSpec>\r\n" + 
        		"	<natures>\r\n" + 
        		"		<nature>org.eclipse.sirius.nature.modelingproject</nature>\r\n" + 
        		"		<nature>org.eclipse.jdt.core.javanature</nature>\r\n" + 
        		"		<nature>org.eclipse.pde.PluginNature</nature>\r\n" + 
        		"		<nature>org.eclipse.xtext.ui.shared.xtextNature</nature>\r\n" + 
        		"	</natures>\r\n" + 
        		"</projectDescription>\r\n" + 
        		"";
                break;
        case ".classpath":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<classpath>\r\n" + 
        		"	<classpathentry kind=\"src\" path=\"src-gen\"/>\r\n" + 
        		"	<classpathentry kind=\"src\" path=\"src\"/>\r\n" + 
        		"	<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.8\"/>\r\n" + 
        		"	<classpathentry kind=\"con\" path=\"org.eclipse.pde.core.requiredPlugins\"/>\r\n" + 
        		"	<classpathentry kind=\"output\" path=\"bin\"/>\r\n" + 
        		"</classpath>";
                break;
        case "aird":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<xmi:XMI xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:description=\"http://www.eclipse.org/sirius/description/1.1.0\" xmlns:description_1=\"http://www.eclipse.org/sirius/diagram/description/1.1.0\" xmlns:diagram=\"http://www.eclipse.org/sirius/diagram/1.1.0\" xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\" xmlns:notation=\"http://www.eclipse.org/gmf/runtime/1.0.2/notation\" xmlns:viewpoint=\"http://www.eclipse.org/sirius/1.1.0\" xsi:schemaLocation=\"http://www.eclipse.org/sirius/description/1.1.0 http://www.eclipse.org/sirius/1.1.0#//description http://www.eclipse.org/sirius/diagram/description/1.1.0 http://www.eclipse.org/sirius/diagram/1.1.0#//description\">\r\n" + 
        		"  <viewpoint:DAnalysis xmi:id=\"_haSxwElOEeizipAf_bMg3A\" selectedViews=\"_hiMJgElOEeizipAf_bMg3A\" version=\"12.1.0.201708031200\">\r\n" + 
        		"    <semanticResources>"+_projectName+".ecore</semanticResources>\r\n" + 
        		"    <semanticResources>"+_projectName+".genmodel</semanticResources>\r\n" + 
        		"    <ownedViews xmi:type=\"viewpoint:DView\" xmi:id=\"_hiMJgElOEeizipAf_bMg3A\">\r\n" + 
        		"      <viewpoint xmi:type=\"description:Viewpoint\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']\"/>\r\n" + 
        		"      <ownedRepresentationDescriptors xmi:type=\"viewpoint:DRepresentationDescriptor\" xmi:id=\"_huvXwElOEeizipAf_bMg3A\" name=\""+_projectName+"\" repPath=\"#_hnPooElOEeizipAf_bMg3A\">\r\n" + 
        		"        <description xmi:type=\"description_1:DiagramDescription\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']/@ownedRepresentations[name='Entities']\"/>\r\n" + 
        		"        <target xmi:type=\"ecore:EPackage\" href=\""+_projectName+".ecore#/\"/>\r\n" + 
        		"      </ownedRepresentationDescriptors>\r\n" + 
        		"    </ownedViews>\r\n" + 
        		"  </viewpoint:DAnalysis>\r\n" + 
        		"  <diagram:DSemanticDiagram xmi:id=\"_hunb8ElOEeizipAf_bMg3A\" name=\""+_projectName+"\" uid=\"_hnPooElOEeizipAf_bMg3A\">\r\n" + 
        		"    <ownedAnnotationEntries xmi:type=\"description:AnnotationEntry\" xmi:id=\"_hunb8UlOEeizipAf_bMg3A\" source=\"DANNOTATION_CUSTOMIZATION_KEY\">\r\n" + 
        		"      <data xmi:type=\"diagram:ComputedStyleDescriptionRegistry\" xmi:id=\"_hunb8klOEeizipAf_bMg3A\"/>\r\n" + 
        		"    </ownedAnnotationEntries>\r\n" + 
        		"    <ownedAnnotationEntries xmi:type=\"description:AnnotationEntry\" xmi:id=\"_hvwrcElOEeizipAf_bMg3A\" source=\"GMF_DIAGRAMS\">\r\n" + 
        		"      <data xmi:type=\"notation:Diagram\" xmi:id=\"_hvwrcUlOEeizipAf_bMg3A\" type=\"Sirius\" element=\"_hunb8ElOEeizipAf_bMg3A\" measurementUnit=\"Pixel\">\r\n" + 
        		"        <styles xmi:type=\"notation:DiagramStyle\" xmi:id=\"_hvwrcklOEeizipAf_bMg3A\"/>\r\n" + 
        		"      </data>\r\n" + 
        		"    </ownedAnnotationEntries>\r\n" + 
        		"    <description xmi:type=\"description_1:DiagramDescription\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']/@ownedRepresentations[name='Entities']\"/>\r\n" + 
        		"    <filterVariableHistory xmi:type=\"diagram:FilterVariableHistory\" xmi:id=\"_hunb9ElOEeizipAf_bMg3A\"/>\r\n" + 
        		"    <activatedLayers xmi:type=\"description_1:Layer\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']/@ownedRepresentations[name='Entities']/@defaultLayer\"/>\r\n" + 
        		"    <activatedLayers xmi:type=\"description_1:AdditionalLayer\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']/@ownedRepresentations[name='Entities']/@additionalLayers[name='Package']\"/>\r\n" + 
        		"    <activatedLayers xmi:type=\"description_1:AdditionalLayer\" href=\"platform:/plugin/org.eclipse.emf.ecoretools.design/description/ecore.odesign#//@ownedViewpoints[name='Design']/@ownedRepresentations[name='Entities']/@additionalLayers[name='Validation']\"/>\r\n" + 
        		"    <target xmi:type=\"ecore:EPackage\" href=\""+_projectName+".ecore#/\"/>\r\n" + 
        		"  </diagram:DSemanticDiagram>\r\n" + 
        		"</xmi:XMI>\r\n" + 
        		"";
                break;
        case "ecore":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<ecore:EPackage xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\"\r\n" + 
        		"    name=\""+_projectName+"\" nsURI=\"http://www.example.org/"+_projectName+"\" nsPrefix=\""+_projectName+"\"/>\r\n" + 
        		"";
                break;
        case "genmodel":  returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
        		"<genmodel:GenModel xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" + 
        		"    xmlns:genmodel=\"http://www.eclipse.org/emf/2002/GenModel\" modelDirectory=\"/"+_projectName+"/src-gen\" creationIcons=\"false\" editDirectory=\"/"+_projectName+".edit/src-gen\"\r\n" + 
        		"    editorDirectory=\"/"+_projectName+".editor/src-gen\" modelPluginID=\""+_projectName+"\" modelName=\""+_projectClass+"\" rootExtendsClass=\"org.eclipse.emf.ecore.impl.MinimalEObjectImpl$Container\"\r\n" + 
        		"    codeFormatting=\"true\" importerID=\"org.eclipse.emf.importer.ecore\" complianceLevel=\"8.0\"\r\n" + 
        		"    copyrightFields=\"false\" operationReflection=\"true\" importOrganizing=\"true\">\r\n" + 
        		"  <foreignModel>"+_projectName+".ecore</foreignModel>\r\n" + 
        		"  <"+_projectName+"sDirectory xsi:nil=\"true\"/>\r\n" + 
        		"  <genPackages prefix=\""+_projectClass+"\" disposableProviderFactory=\"true\" ecorePackage=\""+_projectName+".ecore#/\"/>\r\n" + 
        		"</genmodel:GenModel>\r\n" + 
        		"";
                break;
        case "MANIFEST.MF":  returnValue = "Manifest-Version: 1.0\r\n" + 
        		"Bundle-ManifestVersion: 2\r\n" + 
        		"Bundle-Name: %pluginName\r\n" + 
        		"Bundle-SymbolicName: "+_projectName+";singleton:=true\r\n" + 
        		"Bundle-Version: 0.1.0.qualifier\r\n" + 
        		"Bundle-ClassPath: .\r\n" + 
        		"Bundle-Vendor: %providerName\r\n" + 
        		"Bundle-Localization: plugin\r\n" + 
        		"Bundle-RequiredExecutionEnvironment: JavaSE-1.8\r\n" + 
        		"Export-Package: "+_projectName+",\r\n" + 
        		" "+_projectName+".impl,\r\n" + 
        		" "+_projectName+".util\r\n" + 
        		"Require-Bundle: org.eclipse.emf.ecore;visibility:=reexport,\r\n" + 
        		" org.eclipse.core.runtime,\r\n" + 
        		" org.eclipse.emf.ecore.xmi\r\n" + 
        		"Bundle-ActivationPolicy: lazy\r\n" + 
        		"";
                break;
        case "Xmod_Action.java": returnValue = "/**\r\n" + 
        		" */\r\n" + 
        		"package "+_projectName+";\r\n" + 
        		"\r\n" + 
        		"import org.eclipse.emf.ecore.EObject;\r\n" + 
        		"\r\n" + 
        		"/**\r\n" + 
        		" * <!-- begin-user-doc -->\r\n" + 
        		" * A representation of the model object '<em><b>Xmod Action</b></em>'.\r\n" + 
        		" * <!-- end-user-doc -->\r\n" + 
        		" *\r\n" + 
        		" * <p>\r\n" + 
        		" * The following features are supported:\r\n" + 
        		" * </p>\r\n" + 
        		" * <ul>\r\n" + 
        		" *   <li>{@link "+_projectName+".Xmod_Action#getOnEntry <em>On Entry</em>}</li>\r\n" + 
        		" *   <li>{@link "+_projectName+".Xmod_Action#getOnExit <em>On Exit</em>}</li>\r\n" + 
        		" *   <li>{@link "+_projectName+".Xmod_Action#getOnDo <em>On Do</em>}</li>\r\n" + 
        		" * </ul>\r\n" + 
        		" *\r\n" + 
        		" * @see "+_projectName+"."+_projectClass+"Package#getXmod_Action()\r\n" + 
        		" * @model\r\n" + 
        		" * @generated\r\n" + 
        		" */\r\n" + 
        		"public interface Xmod_Action extends EObject {\r\n" + 
        		"	/**\r\n" + 
        		"	 * Returns the value of the '<em><b>On Entry</b></em>' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <p>\r\n" + 
        		"	 * If the meaning of the '<em>On Entry</em>' containment reference isn't clear,\r\n" + 
        		"	 * there really should be more of a description here...\r\n" + 
        		"	 * </p>\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @return the value of the '<em>On Entry</em>' containment reference.\r\n" + 
        		"	 * @see #setOnEntry(Xmod_Operation)\r\n" + 
        		"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Action_OnEntry()\r\n" + 
        		"	 * @model containment=\"true\"\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	Xmod_Operation getOnEntry();\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * Sets the value of the '{@link "+_projectName+".Xmod_Action#getOnEntry <em>On Entry</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @param value the new value of the '<em>On Entry</em>' containment reference.\r\n" + 
        		"	 * @see #getOnEntry()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void setOnEntry(Xmod_Operation value);\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * Returns the value of the '<em><b>On Exit</b></em>' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <p>\r\n" + 
        		"	 * If the meaning of the '<em>On Exit</em>' containment reference isn't clear,\r\n" + 
        		"	 * there really should be more of a description here...\r\n" + 
        		"	 * </p>\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @return the value of the '<em>On Exit</em>' containment reference.\r\n" + 
        		"	 * @see #setOnExit(Xmod_Operation)\r\n" + 
        		"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Action_OnExit()\r\n" + 
        		"	 * @model containment=\"true\"\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	Xmod_Operation getOnExit();\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * Sets the value of the '{@link "+_projectName+".Xmod_Action#getOnExit <em>On Exit</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @param value the new value of the '<em>On Exit</em>' containment reference.\r\n" + 
        		"	 * @see #getOnExit()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void setOnExit(Xmod_Operation value);\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * Returns the value of the '<em><b>On Do</b></em>' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <p>\r\n" + 
        		"	 * If the meaning of the '<em>On Do</em>' containment reference isn't clear,\r\n" + 
        		"	 * there really should be more of a description here...\r\n" + 
        		"	 * </p>\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @return the value of the '<em>On Do</em>' containment reference.\r\n" + 
        		"	 * @see #setOnDo(Xmod_Operation)\r\n" + 
        		"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Action_OnDo()\r\n" + 
        		"	 * @model containment=\"true\"\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	Xmod_Operation getOnDo();\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * Sets the value of the '{@link "+_projectName+".Xmod_Action#getOnDo <em>On Do</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @param value the new value of the '<em>On Do</em>' containment reference.\r\n" + 
        		"	 * @see #getOnDo()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void setOnDo(Xmod_Operation value);\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * If there is an operation that has to be executed on entry then we call the execute() method.\r\n" +
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see Xmod_Operation#execute()\r\n" +
        		"	 * @model\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void onEntry();\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * If there is an operation that has to be executed on exit then we call the execute() method.\r\n" +
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see Xmod_Operation#execute()\r\n" +
        		"	 * @model\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void onExit();\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * If there is an operation that has to be executed on do then we call the execute() method.\r\n" +
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see Xmod_Operation#execute()\r\n" +
        		"	 * @model\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	void onDo();\r\n" + 
        		"\r\n" + 
        		"} // Xmod_Action\r\n" + 
        		"";
                break;
        case "Xmod_ActionImpl.java": returnValue = "/**\r\n" + 
        		" */\r\n" + 
        		"package "+_projectName+".impl;\r\n" + 
        		"\r\n" + 
        		"import java.lang.reflect.InvocationTargetException;\r\n" + 
        		"\r\n" + 
        		"import org.eclipse.emf.common.notify.Notification;\r\n" + 
        		"\r\n" + 
        		"import org.eclipse.emf.common.notify.NotificationChain;\r\n" + 
        		"import org.eclipse.emf.common.util.EList;\r\n" + 
        		"\r\n" + 
        		"import org.eclipse.emf.ecore.EClass;\r\n" + 
        		"import org.eclipse.emf.ecore.InternalEObject;\r\n" + 
        		"\r\n" + 
        		"import org.eclipse.emf.ecore.impl.ENotificationImpl;\r\n" + 
        		"import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;\r\n" + 
        		"\r\n" + 
        		"import "+_projectName+"."+_projectClass+"Package;\r\n" + 
        		"import "+_projectName+".Xmod_Action;\r\n" + 
        		"import "+_projectName+".Xmod_Operation;\r\n" + 
        		"\r\n" + 
        		"/**\r\n" + 
        		" * <!-- begin-user-doc -->\r\n" + 
        		" * An implementation of the model object '<em><b>Xmod Action</b></em>'.\r\n" + 
        		" * <!-- end-user-doc -->\r\n" + 
        		" * <p>\r\n" + 
        		" * The following features are implemented:\r\n" + 
        		" * </p>\r\n" + 
        		" * <ul>\r\n" + 
        		" *   <li>{@link "+_projectName+".impl.Xmod_ActionImpl#getOnEntry <em>On Entry</em>}</li>\r\n" + 
        		" *   <li>{@link "+_projectName+".impl.Xmod_ActionImpl#getOnExit <em>On Exit</em>}</li>\r\n" + 
        		" *   <li>{@link "+_projectName+".impl.Xmod_ActionImpl#getOnDo <em>On Do</em>}</li>\r\n" + 
        		" * </ul>\r\n" + 
        		" *\r\n" + 
        		" * @generated\r\n" + 
        		" */\r\n" + 
        		"public class Xmod_ActionImpl extends MinimalEObjectImpl.Container implements Xmod_Action {\r\n" + 
        		"	/**\r\n" + 
        		"	 * The cached value of the '{@link #getOnEntry() <em>On Entry</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see #getOnEntry()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 * @ordered\r\n" + 
        		"	 */\r\n" + 
        		"	protected Xmod_Operation onEntry;\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * The cached value of the '{@link #getOnExit() <em>On Exit</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see #getOnExit()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 * @ordered\r\n" + 
        		"	 */\r\n" + 
        		"	protected Xmod_Operation onExit;\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * The cached value of the '{@link #getOnDo() <em>On Do</em>}' containment reference.\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see #getOnDo()\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 * @ordered\r\n" + 
        		"	 */\r\n" + 
        		"	protected Xmod_Operation onDo;\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	protected Xmod_ActionImpl() {\r\n" + 
        		"		super();\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	protected EClass eStaticClass() {\r\n" + 
        		"		return "+_projectClass+"Package.Literals.XMOD_ACTION;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public Xmod_Operation getOnEntry() {\r\n" + 
        		"		return onEntry;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public NotificationChain basicSetOnEntry(Xmod_Operation newOnEntry, NotificationChain msgs) {\r\n" + 
        		"		Xmod_Operation oldOnEntry = onEntry;\r\n" + 
        		"		onEntry = newOnEntry;\r\n" + 
        		"		if (eNotificationRequired()) {\r\n" + 
        		"			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,\r\n" + 
        		"					"+_projectClass+"Package.XMOD_ACTION__ON_ENTRY, oldOnEntry, newOnEntry);\r\n" + 
        		"			if (msgs == null)\r\n" + 
        		"				msgs = notification;\r\n" + 
        		"			else\r\n" + 
        		"				msgs.add(notification);\r\n" + 
        		"		}\r\n" + 
        		"		return msgs;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public void setOnEntry(Xmod_Operation newOnEntry) {\r\n" + 
        		"		if (newOnEntry != onEntry) {\r\n" + 
        		"			NotificationChain msgs = null;\r\n" + 
        		"			if (onEntry != null)\r\n" + 
        		"				msgs = ((InternalEObject) onEntry).eInverseRemove(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY, null, msgs);\r\n" + 
        		"			if (newOnEntry != null)\r\n" + 
        		"				msgs = ((InternalEObject) newOnEntry).eInverseAdd(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY, null, msgs);\r\n" + 
        		"			msgs = basicSetOnEntry(newOnEntry, msgs);\r\n" + 
        		"			if (msgs != null)\r\n" + 
        		"				msgs.dispatch();\r\n" + 
        		"		} else if (eNotificationRequired())\r\n" + 
        		"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY, newOnEntry,\r\n" + 
        		"					newOnEntry));\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public Xmod_Operation getOnExit() {\r\n" + 
        		"		return onExit;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public NotificationChain basicSetOnExit(Xmod_Operation newOnExit, NotificationChain msgs) {\r\n" + 
        		"		Xmod_Operation oldOnExit = onExit;\r\n" + 
        		"		onExit = newOnExit;\r\n" + 
        		"		if (eNotificationRequired()) {\r\n" + 
        		"			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,\r\n" + 
        		"					"+_projectClass+"Package.XMOD_ACTION__ON_EXIT, oldOnExit, newOnExit);\r\n" + 
        		"			if (msgs == null)\r\n" + 
        		"				msgs = notification;\r\n" + 
        		"			else\r\n" + 
        		"				msgs.add(notification);\r\n" + 
        		"		}\r\n" + 
        		"		return msgs;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public void setOnExit(Xmod_Operation newOnExit) {\r\n" + 
        		"		if (newOnExit != onExit) {\r\n" + 
        		"			NotificationChain msgs = null;\r\n" + 
        		"			if (onExit != null)\r\n" + 
        		"				msgs = ((InternalEObject) onExit).eInverseRemove(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_EXIT, null, msgs);\r\n" + 
        		"			if (newOnExit != null)\r\n" + 
        		"				msgs = ((InternalEObject) newOnExit).eInverseAdd(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_EXIT, null, msgs);\r\n" + 
        		"			msgs = basicSetOnExit(newOnExit, msgs);\r\n" + 
        		"			if (msgs != null)\r\n" + 
        		"				msgs.dispatch();\r\n" + 
        		"		} else if (eNotificationRequired())\r\n" + 
        		"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_ACTION__ON_EXIT, newOnExit,\r\n" + 
        		"					newOnExit));\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public Xmod_Operation getOnDo() {\r\n" + 
        		"		return onDo;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public NotificationChain basicSetOnDo(Xmod_Operation newOnDo, NotificationChain msgs) {\r\n" + 
        		"		Xmod_Operation oldOnDo = onDo;\r\n" + 
        		"		onDo = newOnDo;\r\n" + 
        		"		if (eNotificationRequired()) {\r\n" + 
        		"			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,\r\n" + 
        		"					"+_projectClass+"Package.XMOD_ACTION__ON_DO, oldOnDo, newOnDo);\r\n" + 
        		"			if (msgs == null)\r\n" + 
        		"				msgs = notification;\r\n" + 
        		"			else\r\n" + 
        		"				msgs.add(notification);\r\n" + 
        		"		}\r\n" + 
        		"		return msgs;\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	public void setOnDo(Xmod_Operation newOnDo) {\r\n" + 
        		"		if (newOnDo != onDo) {\r\n" + 
        		"			NotificationChain msgs = null;\r\n" + 
        		"			if (onDo != null)\r\n" + 
        		"				msgs = ((InternalEObject) onDo).eInverseRemove(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_DO, null, msgs);\r\n" + 
        		"			if (newOnDo != null)\r\n" + 
        		"				msgs = ((InternalEObject) newOnDo).eInverseAdd(this,\r\n" + 
        		"						EOPPOSITE_FEATURE_BASE - "+_projectClass+"Package.XMOD_ACTION__ON_DO, null, msgs);\r\n" + 
        		"			msgs = basicSetOnDo(newOnDo, msgs);\r\n" + 
        		"			if (msgs != null)\r\n" + 
        		"				msgs.dispatch();\r\n" + 
        		"		} else if (eNotificationRequired())\r\n" + 
        		"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_ACTION__ON_DO, newOnDo, newOnDo));\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * If there is an operation that has to be executed on entry then we call the execute() method.\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @see Xmod_OperationImpl#execute()\r\n" +
        		"	 * @generated NOT\r\n" + 
        		"	 */\r\n" + 
        		"	public void onEntry() {\r\n" + 
        		"		// TODO: \r\n" + 
        		"		if (getOnEntry() != null) {\r\n" + 
        		"			getOnEntry().execute();\r\n" + 
        		"		}\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * If there is an operation that has to be executed on exit then we call the execute() method.\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" +  
        		"	 * @see Xmod_OperationImpl#execute()\r\n" +
        		"	 * @generated NOT\r\n" + 
        		"	 */\r\n" + 
        		"	public void onExit() {\r\n" + 
        		"		// TODO: \r\n" + 
        		"		if (getOnExit() != null) {\r\n" + 
        		"			getOnExit().execute();\r\n" + 
        		"		}\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * If there is an operation that has to be executed on do then we call the execute() method.\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" +  
        		"	 * @see Xmod_OperationImpl#execute()\r\n" +
        		"	 * @generated NOT\r\n" + 
        		"	 */\r\n" + 
        		"	public void onDo() {\r\n" + 
        		"		// TODO\r\n" + 
        		"		if (getOnDo() != null) {\r\n" + 
        		"			getOnDo().execute();\r\n" + 
        		"		}\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {\r\n" + 
        		"		switch (featureID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY:\r\n" + 
        		"			return basicSetOnEntry(null, msgs);\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_EXIT:\r\n" + 
        		"			return basicSetOnExit(null, msgs);\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_DO:\r\n" + 
        		"			return basicSetOnDo(null, msgs);\r\n" + 
        		"		}\r\n" + 
        		"		return super.eInverseRemove(otherEnd, featureID, msgs);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public Object eGet(int featureID, boolean resolve, boolean coreType) {\r\n" + 
        		"		switch (featureID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY:\r\n" + 
        		"			return getOnEntry();\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_EXIT:\r\n" + 
        		"			return getOnExit();\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_DO:\r\n" + 
        		"			return getOnDo();\r\n" + 
        		"		}\r\n" + 
        		"		return super.eGet(featureID, resolve, coreType);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public void eSet(int featureID, Object newValue) {\r\n" + 
        		"		switch (featureID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY:\r\n" + 
        		"			setOnEntry((Xmod_Operation) newValue);\r\n" + 
        		"			return;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_EXIT:\r\n" + 
        		"			setOnExit((Xmod_Operation) newValue);\r\n" + 
        		"			return;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_DO:\r\n" + 
        		"			setOnDo((Xmod_Operation) newValue);\r\n" + 
        		"			return;\r\n" + 
        		"		}\r\n" + 
        		"		super.eSet(featureID, newValue);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public void eUnset(int featureID) {\r\n" + 
        		"		switch (featureID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY:\r\n" + 
        		"			setOnEntry((Xmod_Operation) null);\r\n" + 
        		"			return;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_EXIT:\r\n" + 
        		"			setOnExit((Xmod_Operation) null);\r\n" + 
        		"			return;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_DO:\r\n" + 
        		"			setOnDo((Xmod_Operation) null);\r\n" + 
        		"			return;\r\n" + 
        		"		}\r\n" + 
        		"		super.eUnset(featureID);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public boolean eIsSet(int featureID) {\r\n" + 
        		"		switch (featureID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_ENTRY:\r\n" + 
        		"			return onEntry != null;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_EXIT:\r\n" + 
        		"			return onExit != null;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION__ON_DO:\r\n" + 
        		"			return onDo != null;\r\n" + 
        		"		}\r\n" + 
        		"		return super.eIsSet(featureID);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"	/**\r\n" + 
        		"	 * <!-- begin-user-doc -->\r\n" + 
        		"	 * <!-- end-user-doc -->\r\n" + 
        		"	 * @generated\r\n" + 
        		"	 */\r\n" + 
        		"	@Override\r\n" + 
        		"	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {\r\n" + 
        		"		switch (operationID) {\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION___ON_ENTRY:\r\n" + 
        		"			onEntry();\r\n" + 
        		"			return null;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION___ON_EXIT:\r\n" + 
        		"			onExit();\r\n" + 
        		"			return null;\r\n" + 
        		"		case "+_projectClass+"Package.XMOD_ACTION___ON_DO:\r\n" + 
        		"			onDo();\r\n" + 
        		"			return null;\r\n" + 
        		"		}\r\n" + 
        		"		return super.eInvoke(operationID, arguments);\r\n" + 
        		"	}\r\n" + 
        		"\r\n" + 
        		"} //Xmod_ActionImpl\r\n" + 
        		"";
                break;
        case "Xmod_Operation.java":  returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+";\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.common.util.EList;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EObject;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * A representation of the model object '<em><b>Xmod Operation</b></em>'.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" *\r\n" + 
				" * <p>\r\n" + 
				" * The following features are supported:\r\n" + 
				" * </p>\r\n" + 
				" * <ul>\r\n" + 
				" *   <li>{@link "+_projectName+".Xmod_Operation#getName <em>Name</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".Xmod_Operation#getObjectTag <em>Object Tag</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".Xmod_Operation#getParametersTag <em>Parameters Tag</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".Xmod_Operation#getReturnTag <em>Return Tag</em>}</li>\r\n" + 
				" * </ul>\r\n" + 
				" *\r\n" + 
				" * @see "+_projectName+"."+_projectClass+"Package#getXmod_Operation()\r\n" + 
				" * @model\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public interface Xmod_Operation extends EObject {\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the value of the '<em><b>Name</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <p>\r\n" + 
				"	 * If the meaning of the '<em>Name</em>' attribute isn't clear,\r\n" + 
				"	 * there really should be more of a description here...\r\n" + 
				"	 * </p>\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the value of the '<em>Name</em>' attribute.\r\n" + 
				"	 * @see #setName(String)\r\n" + 
				"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Operation_Name()\r\n" + 
				"	 * @model\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String getName();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Sets the value of the '{@link "+_projectName+".Xmod_Operation#getName <em>Name</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param value the new value of the '<em>Name</em>' attribute.\r\n" + 
				"	 * @see #getName()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	void setName(String value);\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the value of the '<em><b>Object Tag</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <p>\r\n" + 
				"	 * If the meaning of the '<em>Object Tag</em>' attribute isn't clear,\r\n" + 
				"	 * there really should be more of a description here...\r\n" + 
				"	 * </p>\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the value of the '<em>Object Tag</em>' attribute.\r\n" + 
				"	 * @see #setObjectTag(String)\r\n" + 
				"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Operation_ObjectTag()\r\n" + 
				"	 * @model required=\"true\"\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String getObjectTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Sets the value of the '{@link "+_projectName+".Xmod_Operation#getObjectTag <em>Object Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param value the new value of the '<em>Object Tag</em>' attribute.\r\n" + 
				"	 * @see #getObjectTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	void setObjectTag(String value);\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the value of the '<em><b>Parameters Tag</b></em>' attribute list.\r\n" + 
				"	 * The list contents are of type {@link java.lang.String}.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <p>\r\n" + 
				"	 * If the meaning of the '<em>Parameters Tag</em>' attribute list isn't clear,\r\n" + 
				"	 * there really should be more of a description here...\r\n" + 
				"	 * </p>\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the value of the '<em>Parameters Tag</em>' attribute list.\r\n" + 
				"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Operation_ParametersTag()\r\n" + 
				"	 * @model\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EList<String> getParametersTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the value of the '<em><b>Return Tag</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <p>\r\n" + 
				"	 * If the meaning of the '<em>Return Tag</em>' attribute isn't clear,\r\n" + 
				"	 * there really should be more of a description here...\r\n" + 
				"	 * </p>\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the value of the '<em>Return Tag</em>' attribute.\r\n" + 
				"	 * @see #setReturnTag(String)\r\n" + 
				"	 * @see "+_projectName+"."+_projectClass+"Package#getXmod_Operation_ReturnTag()\r\n" + 
				"	 * @model\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String getReturnTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Sets the value of the '{@link "+_projectName+".Xmod_Operation#getReturnTag <em>Return Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param value the new value of the '<em>Return Tag</em>' attribute.\r\n" + 
				"	 * @see #getReturnTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	void setReturnTag(String value);\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Execution by dynamic invocation of method.\r\n" +
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getObjectTag()\r\n" + 
				"	 * @see #getParametersTag()\r\n" + 
				"	 * @see #getReturnTag()\r\n" +
				"	 * @model\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	void execute();\r\n" + 
				"\r\n" + 
				"} // Xmod_Operation\r\n" + 
				"";
		        break;
		case "Xmod_OperationImpl.java":  returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+".impl;\r\n" + 
				"\r\n" + 
				"import java.lang.reflect.InvocationTargetException;\r\n" + 
				"import java.lang.reflect.Method;\r\n" + 
				"import java.lang.reflect.Modifier;\r\n" + 
				"import java.util.Collection;\r\n" + 
				"import java.util.HashMap;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.common.notify.Notification;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.common.util.EList;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EClass;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.impl.ENotificationImpl;\r\n" + 
				"import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;\r\n" + 
				"\r\n" + 
				"import "+_projectName+"."+_projectClass+"Package;\r\n" + 
				"import "+_projectName+".Xmod_Operation;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * An implementation of the model object '<em><b>Xmod Operation</b></em>'.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * <p>\r\n" + 
				" * The following features are implemented:\r\n" + 
				" * </p>\r\n" + 
				" * <ul>\r\n" + 
				" *   <li>{@link "+_projectName+".impl.Xmod_OperationImpl#getName <em>Name</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".impl.Xmod_OperationImpl#getObjectTag <em>Object Tag</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".impl.Xmod_OperationImpl#getParametersTag <em>Parameters Tag</em>}</li>\r\n" + 
				" *   <li>{@link "+_projectName+".impl.Xmod_OperationImpl#getReturnTag <em>Return Tag</em>}</li>\r\n" + 
				" * </ul>\r\n" + 
				" *\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public class Xmod_OperationImpl extends MinimalEObjectImpl.Container implements Xmod_Operation {\r\n" + 
				"	/**\r\n" + 
				"	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getName()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected static final String NAME_EDEFAULT = null;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getName()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected String name = NAME_EDEFAULT;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The default value of the '{@link #getObjectTag() <em>Object Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getObjectTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected static final String OBJECT_TAG_EDEFAULT = null;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The cached value of the '{@link #getObjectTag() <em>Object Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getObjectTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected String objectTag = OBJECT_TAG_EDEFAULT;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The cached value of the '{@link #getParametersTag() <em>Parameters Tag</em>}' attribute list.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getParametersTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected EList<String> parametersTag;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The default value of the '{@link #getReturnTag() <em>Return Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getReturnTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected static final String RETURN_TAG_EDEFAULT = null;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The cached value of the '{@link #getReturnTag() <em>Return Tag</em>}' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #getReturnTag()\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	protected String returnTag = RETURN_TAG_EDEFAULT;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	protected Xmod_OperationImpl() {\r\n" + 
				"		super();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	protected EClass eStaticClass() {\r\n" + 
				"		return "+_projectClass+"Package.Literals.XMOD_OPERATION;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public String getName() {\r\n" + 
				"		return name;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public void setName(String newName) {\r\n" + 
				"		String oldName = name;\r\n" + 
				"		name = newName;\r\n" + 
				"		if (eNotificationRequired())\r\n" + 
				"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_OPERATION__NAME, oldName, name));\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public String getObjectTag() {\r\n" + 
				"		return objectTag;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public void setObjectTag(String newObjectTag) {\r\n" + 
				"		String oldObjectTag = objectTag;\r\n" + 
				"		objectTag = newObjectTag;\r\n" + 
				"		if (eNotificationRequired())\r\n" + 
				"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_OPERATION__OBJECT_TAG, oldObjectTag,\r\n" + 
				"					objectTag));\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EList<String> getParametersTag() {\r\n" + 
				"		if (parametersTag == null) {\r\n" + 
				"			parametersTag = new EDataTypeUniqueEList<String>(String.class, this,\r\n" + 
				"					"+_projectClass+"Package.XMOD_OPERATION__PARAMETERS_TAG);\r\n" + 
				"		}\r\n" + 
				"		return parametersTag;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public String getReturnTag() {\r\n" + 
				"		return returnTag;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public void setReturnTag(String newReturnTag) {\r\n" + 
				"		String oldReturnTag = returnTag;\r\n" + 
				"		returnTag = newReturnTag;\r\n" + 
				"		if (eNotificationRequired())\r\n" + 
				"			eNotify(new ENotificationImpl(this, Notification.SET, "+_projectClass+"Package.XMOD_OPERATION__RETURN_TAG, oldReturnTag,\r\n" + 
				"					returnTag));\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"   /**\r\n" + 
				"	 * Solve the limitation of the reflectiviy API when looking for intern class or interface.\r\n" +
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated NOT\r\n" + 
				"	 */\r\n" + 
				"   @SuppressWarnings(\"rawtypes\")\r\n" +
				"	private static Method findHighestMethod(Class cls, String method) {\r\n" + 
				"		\r\n" + 
				"		/* Init */\r\n" + 
				"		Class[] ifaces; //interfaces\r\n" + 
				"		Method ifaceMethod;\r\n" + 
				"		Method parentMethod;\r\n" + 
				"		Method[] methods;\r\n" + 
				"		\r\n" + 
				"		/* Body */\r\n" + 
				"		ifaces = cls.getInterfaces();\r\n" + 
				"		for (int i = 0; i < ifaces.length; i++) {\r\n" + 
				"			ifaceMethod = findHighestMethod(ifaces[i], method);\r\n" + 
				"			if (ifaceMethod != null) return ifaceMethod;\r\n" + 
				"		}\r\n" + 
				"		if (cls.getSuperclass() != null) {\r\n" + 
				"			parentMethod = findHighestMethod(\r\n" + 
				"					cls.getSuperclass(), method);\r\n" + 
				"			if (parentMethod != null) return parentMethod;\r\n" + 
				"		}\r\n" + 
				"		methods  = cls.getMethods();\r\n" + 
				"		for (int i = 0; i < methods.length; i++) {\r\n" + 
				"			if (methods[i].getName().equals(method)) {\r\n" + 
				"				return methods[i];\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" +
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Execution by dynamic invocation of method.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #objectTag\r\n" + 
				"	 * @see #parametersTag\r\n" + 
				"	 * @see #returnTag\r\n" + 
				"	 * @see #getObjectTag()\r\n" + 
				"	 * @see #getParametersTag()\r\n" + 
				"	 * @see #getReturnTag()\r\n" + 
				"	 * @see #findHighestMethod(Class, String)\r\n" + 
				"	 * @generated NOT\r\n" + 
				"	 */\r\n" + 
				"	public void execute() {\r\n" + 
				"		// TODO: \r\n" + 
				"\r\n" + 
				"		/* Init */\r\n" + 
				"		HashMap<String, Object> hashMap;\r\n" + 
				"		Object returnValue; // Returned object by the method\r\n" + 
				"		Class<?>[] parametersType = null;\r\n" + 
				"		Method m;\r\n" + 
				"		Object[] parameters = null;\r\n" + 
				"\r\n" + 
				"		/* Body */\r\n" + 
				"		hashMap = (HashMap<String, Object>) "+_projectName+".util."+_projectClass+"XmodUtil.getMap();\r\n" + 
				"		if (getObjectTag() != null) { // check if the object on which the operation is being called has an annotation\r\n" + 
				"			// in case there are one or more parameters\r\n" + 
				"			if (getParametersTag() != null) {\r\n" + 
				"				parametersType = new Class[getParametersTag().size()];\r\n" + 
				"				parameters = new Object[getParametersTag().size()];\r\n" + 
				"				// browsing of the list of parameters of the operation\r\n" + 
				"				for (int i = 0; i < getParametersTag().size(); i++) {\r\n" + 
				"					if (getParametersTag().get(i) != null) { // check if the item has an annotation\r\n" + 
				"						// get the type of the item\r\n" + 
				"						parametersType[i] = hashMap.get(getParametersTag().get(i)).getClass();\r\n" + 
				"						// get the corresponding value of the item in the hashMap\r\n" + 
				"						parameters[i] = hashMap.get(getParametersTag().get(i));\r\n" + 
				"					}\r\n" + 
				"				}\r\n" + 
				"			}\r\n" + 
				"			try {\r\n" + 
				"				// dynamic invocation\r\n" + 
				"				m = findHighestMethod(hashMap.get(getObjectTag()).getClass(), getName());\r\n" + 
				"				if (Modifier.isStatic(m.getModifiers())) {\r\n" + 
				"					returnValue = m.invoke(null, parameters);\r\n" + 
				"				} else {\r\n" + 
				"					returnValue = m.invoke(hashMap.get(getObjectTag()), parameters);\r\n" + 
				"				}\r\n" + 
				"				// update of the hashmap\r\n" + 
				"				if (getReturnTag() != null) {\r\n" + 
				"					hashMap.put(getReturnTag(), returnValue);\r\n" + 
				"				}\r\n" + 
				"			} catch (SecurityException e) {\r\n" + 
				"				e.printStackTrace();\r\n" + 
				"			} catch (IllegalAccessException e) {\r\n" + 
				"				e.printStackTrace();\r\n" + 
				"			} catch (IllegalArgumentException e) {\r\n" + 
				"				e.printStackTrace();\r\n" + 
				"			} catch (InvocationTargetException e) {\r\n" + 
				"				e.printStackTrace();\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public Object eGet(int featureID, boolean resolve, boolean coreType) {\r\n" + 
				"		switch (featureID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__NAME:\r\n" + 
				"			return getName();\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__OBJECT_TAG:\r\n" + 
				"			return getObjectTag();\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__PARAMETERS_TAG:\r\n" + 
				"			return getParametersTag();\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__RETURN_TAG:\r\n" + 
				"			return getReturnTag();\r\n" + 
				"		}\r\n" + 
				"		return super.eGet(featureID, resolve, coreType);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@SuppressWarnings(\"unchecked\")\r\n" + 
				"	@Override\r\n" + 
				"	public void eSet(int featureID, Object newValue) {\r\n" + 
				"		switch (featureID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__NAME:\r\n" + 
				"			setName((String) newValue);\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__OBJECT_TAG:\r\n" + 
				"			setObjectTag((String) newValue);\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__PARAMETERS_TAG:\r\n" + 
				"			getParametersTag().clear();\r\n" + 
				"			getParametersTag().addAll((Collection<? extends String>) newValue);\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__RETURN_TAG:\r\n" + 
				"			setReturnTag((String) newValue);\r\n" + 
				"			return;\r\n" + 
				"		}\r\n" + 
				"		super.eSet(featureID, newValue);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public void eUnset(int featureID) {\r\n" + 
				"		switch (featureID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__NAME:\r\n" + 
				"			setName(NAME_EDEFAULT);\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__OBJECT_TAG:\r\n" + 
				"			setObjectTag(OBJECT_TAG_EDEFAULT);\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__PARAMETERS_TAG:\r\n" + 
				"			getParametersTag().clear();\r\n" + 
				"			return;\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__RETURN_TAG:\r\n" + 
				"			setReturnTag(RETURN_TAG_EDEFAULT);\r\n" + 
				"			return;\r\n" + 
				"		}\r\n" + 
				"		super.eUnset(featureID);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public boolean eIsSet(int featureID) {\r\n" + 
				"		switch (featureID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__NAME:\r\n" + 
				"			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__OBJECT_TAG:\r\n" + 
				"			return OBJECT_TAG_EDEFAULT == null ? objectTag != null : !OBJECT_TAG_EDEFAULT.equals(objectTag);\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__PARAMETERS_TAG:\r\n" + 
				"			return parametersTag != null && !parametersTag.isEmpty();\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION__RETURN_TAG:\r\n" + 
				"			return RETURN_TAG_EDEFAULT == null ? returnTag != null : !RETURN_TAG_EDEFAULT.equals(returnTag);\r\n" + 
				"		}\r\n" + 
				"		return super.eIsSet(featureID);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {\r\n" + 
				"		switch (operationID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION___EXECUTE:\r\n" + 
				"			execute();\r\n" + 
				"			return null;\r\n" + 
				"		}\r\n" + 
				"		return super.eInvoke(operationID, arguments);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public String toString() {\r\n" + 
				"		if (eIsProxy())\r\n" + 
				"			return super.toString();\r\n" + 
				"\r\n" + 
				"		StringBuffer result = new StringBuffer(super.toString());\r\n" + 
				"		result.append(\" (name: \");\r\n" + 
				"		result.append(name);\r\n" + 
				"		result.append(\", objectTag: \");\r\n" + 
				"		result.append(objectTag);\r\n" + 
				"		result.append(\", parametersTag: \");\r\n" + 
				"		result.append(parametersTag);\r\n" + 
				"		result.append(\", returnTag: \");\r\n" + 
				"		result.append(returnTag);\r\n" + 
				"		result.append(')');\r\n" + 
				"		return result.toString();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //Xmod_OperationImpl\r\n" + 
				"";
		        break;
		case "Factory.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+";\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EFactory;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * The <b>Factory</b> for the model.\r\n" + 
				" * It provides a create method for each non-abstract class of the model.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @see "+_projectName+"."+_projectClass+"Package\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public interface "+_projectClass+"Factory extends EFactory {\r\n" + 
				"	/**\r\n" + 
				"	 * The singleton instance of the factory.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	"+_projectClass+"Factory eINSTANCE = "+_projectName+".impl."+_projectClass+"FactoryImpl.init();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns a new object of class '<em>Xmod Operation</em>'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return a new object of class '<em>Xmod Operation</em>'.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	Xmod_Operation createXmod_Operation();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns a new object of class '<em>Xmod Action</em>'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return a new object of class '<em>Xmod Action</em>'.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	Xmod_Action createXmod_Action();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the package supported by this factory.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the package supported by this factory.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	"+_projectClass+"Package get"+_projectClass+"Package();\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"Factory\r\n" + 
				"";
		        break;
		case "FactoryImpl.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+".impl;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EClass;\r\n" + 
				"import org.eclipse.emf.ecore.EObject;\r\n" + 
				"import org.eclipse.emf.ecore.EPackage;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.impl.EFactoryImpl;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.plugin.EcorePlugin;\r\n" + 
				"\r\n" + 
				"import "+_projectName+".*;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * An implementation of the model <b>Factory</b>.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public class "+_projectClass+"FactoryImpl extends EFactoryImpl implements "+_projectClass+"Factory {\r\n" + 
				"	/**\r\n" + 
				"	 * Creates the default factory implementation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public static "+_projectClass+"Factory init() {\r\n" + 
				"		try {\r\n" + 
				"			"+_projectClass+"Factory the"+_projectClass+"Factory = ("+_projectClass+"Factory) EPackage.Registry.INSTANCE.getEFactory("+_projectClass+"Package.eNS_URI);\r\n" + 
				"			if (the"+_projectClass+"Factory != null) {\r\n" + 
				"				return the"+_projectClass+"Factory;\r\n" + 
				"			}\r\n" + 
				"		} catch (Exception exception) {\r\n" + 
				"			EcorePlugin.INSTANCE.log(exception);\r\n" + 
				"		}\r\n" + 
				"		return new "+_projectClass+"FactoryImpl();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates an instance of the factory.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public "+_projectClass+"FactoryImpl() {\r\n" + 
				"		super();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public EObject create(EClass eClass) {\r\n" + 
				"		switch (eClass.getClassifierID()) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION:\r\n" + 
				"			return createXmod_Operation();\r\n" + 
				"		case "+_projectClass+"Package.XMOD_ACTION:\r\n" + 
				"			return createXmod_Action();\r\n" + 
				"		default:\r\n" + 
				"			throw new IllegalArgumentException(\"The class '\" + eClass.getName() + \"' is not a valid classifier\");\r\n" + 
				"		}\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public Xmod_Operation createXmod_Operation() {\r\n" + 
				"		Xmod_OperationImpl xmod_Operation = new Xmod_OperationImpl();\r\n" + 
				"		return xmod_Operation;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public Xmod_Action createXmod_Action() {\r\n" + 
				"		Xmod_ActionImpl xmod_Action = new Xmod_ActionImpl();\r\n" + 
				"		return xmod_Action;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public "+_projectClass+"Package get"+_projectClass+"Package() {\r\n" + 
				"		return ("+_projectClass+"Package) getEPackage();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @deprecated\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Deprecated\r\n" + 
				"	public static "+_projectClass+"Package getPackage() {\r\n" + 
				"		return "+_projectClass+"Package.eINSTANCE;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"FactoryImpl\r\n" + 
				"";
		        break;
		case "Package.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+";\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EAttribute;\r\n" + 
				"import org.eclipse.emf.ecore.EClass;\r\n" + 
				"import org.eclipse.emf.ecore.EOperation;\r\n" + 
				"import org.eclipse.emf.ecore.EPackage;\r\n" + 
				"import org.eclipse.emf.ecore.EReference;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * The <b>Package</b> for the model.\r\n" + 
				" * It contains accessors for the meta objects to represent\r\n" + 
				" * <ul>\r\n" + 
				" *   <li>each class,</li>\r\n" + 
				" *   <li>each feature of each class,</li>\r\n" + 
				" *   <li>each operation of each class,</li>\r\n" + 
				" *   <li>each enum,</li>\r\n" + 
				" *   <li>and each data type</li>\r\n" + 
				" * </ul>\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @see "+_projectName+"."+_projectClass+"Factory\r\n" + 
				" * @model kind=\"package\"\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public interface "+_projectClass+"Package extends EPackage {\r\n" + 
				"	/**\r\n" + 
				"	 * The package name.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String eNAME = \""+_projectClass+"\";\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The package namespace URI.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String eNS_URI = \"platform:/resource/"+_projectClass+"Exec/model/"+_projectClass+".ecore\";\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The package namespace name.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	String eNS_PREFIX = \""+_projectClass+"\";\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The singleton instance of the package.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	"+_projectClass+"Package eINSTANCE = "+_projectName+".impl."+_projectClass+"PackageImpl.init();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The meta object id for the '{@link "+_projectName+".impl.Xmod_OperationImpl <em>Xmod Operation</em>}' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see "+_projectName+".impl.Xmod_OperationImpl\r\n" + 
				"	 * @see "+_projectName+".impl."+_projectClass+"PackageImpl#getXmod_Operation()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION = 0;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>Name</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION__NAME = 0;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>Object Tag</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION__OBJECT_TAG = 1;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>Parameters Tag</b></em>' attribute list.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION__PARAMETERS_TAG = 2;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>Return Tag</b></em>' attribute.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION__RETURN_TAG = 3;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The number of structural features of the '<em>Xmod Operation</em>' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION_FEATURE_COUNT = 4;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The operation id for the '<em>Execute</em>' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION___EXECUTE = 0;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The number of operations of the '<em>Xmod Operation</em>' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_OPERATION_OPERATION_COUNT = 1;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The meta object id for the '{@link "+_projectName+".impl.Xmod_ActionImpl <em>Xmod Action</em>}' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see "+_projectName+".impl.Xmod_ActionImpl\r\n" + 
				"	 * @see "+_projectName+".impl."+_projectClass+"PackageImpl#getXmod_Action()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION = 1;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>On Entry</b></em>' containment reference.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION__ON_ENTRY = 0;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>On Exit</b></em>' containment reference.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION__ON_EXIT = 1;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The feature id for the '<em><b>On Do</b></em>' containment reference.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION__ON_DO = 2;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The number of structural features of the '<em>Xmod Action</em>' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION_FEATURE_COUNT = 3;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The operation id for the '<em>On Entry</em>' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION___ON_ENTRY = 0;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The operation id for the '<em>On Exit</em>' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION___ON_EXIT = 1;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The operation id for the '<em>On Do</em>' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION___ON_DO = 2;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The number of operations of the '<em>Xmod Action</em>' class.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 * @ordered\r\n" + 
				"	 */\r\n" + 
				"	int XMOD_ACTION_OPERATION_COUNT = 3;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for class '{@link "+_projectName+".Xmod_Operation <em>Xmod Operation</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for class '<em>Xmod Operation</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EClass getXmod_Operation();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the attribute '{@link "+_projectName+".Xmod_Operation#getName <em>Name</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the attribute '<em>Name</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation#getName()\r\n" + 
				"	 * @see #getXmod_Operation()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EAttribute getXmod_Operation_Name();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the attribute '{@link "+_projectName+".Xmod_Operation#getObjectTag <em>Object Tag</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the attribute '<em>Object Tag</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation#getObjectTag()\r\n" + 
				"	 * @see #getXmod_Operation()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EAttribute getXmod_Operation_ObjectTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the attribute list '{@link "+_projectName+".Xmod_Operation#getParametersTag <em>Parameters Tag</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the attribute list '<em>Parameters Tag</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation#getParametersTag()\r\n" + 
				"	 * @see #getXmod_Operation()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EAttribute getXmod_Operation_ParametersTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the attribute '{@link "+_projectName+".Xmod_Operation#getReturnTag <em>Return Tag</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the attribute '<em>Return Tag</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation#getReturnTag()\r\n" + 
				"	 * @see #getXmod_Operation()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EAttribute getXmod_Operation_ReturnTag();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the '{@link "+_projectName+".Xmod_Operation#execute() <em>Execute</em>}' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the '<em>Execute</em>' operation.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation#execute()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EOperation getXmod_Operation__Execute();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for class '{@link "+_projectName+".Xmod_Action <em>Xmod Action</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for class '<em>Xmod Action</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EClass getXmod_Action();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the containment reference '{@link "+_projectName+".Xmod_Action#getOnEntry <em>On Entry</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the containment reference '<em>On Entry</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#getOnEntry()\r\n" + 
				"	 * @see #getXmod_Action()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EReference getXmod_Action_OnEntry();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the containment reference '{@link "+_projectName+".Xmod_Action#getOnExit <em>On Exit</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the containment reference '<em>On Exit</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#getOnExit()\r\n" + 
				"	 * @see #getXmod_Action()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EReference getXmod_Action_OnExit();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the containment reference '{@link "+_projectName+".Xmod_Action#getOnDo <em>On Do</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the containment reference '<em>On Do</em>'.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#getOnDo()\r\n" + 
				"	 * @see #getXmod_Action()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EReference getXmod_Action_OnDo();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the '{@link "+_projectName+".Xmod_Action#onEntry() <em>On Entry</em>}' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the '<em>On Entry</em>' operation.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#onEntry()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EOperation getXmod_Action__OnEntry();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the '{@link "+_projectName+".Xmod_Action#onExit() <em>On Exit</em>}' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the '<em>On Exit</em>' operation.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#onExit()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EOperation getXmod_Action__OnExit();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the meta object for the '{@link "+_projectName+".Xmod_Action#onDo() <em>On Do</em>}' operation.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the meta object for the '<em>On Do</em>' operation.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action#onDo()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	EOperation getXmod_Action__OnDo();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the factory that creates the instances of the model.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the factory that creates the instances of the model.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	"+_projectClass+"Factory get"+_projectClass+"Factory();\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * Defines literals for the meta objects that represent\r\n" + 
				"	 * <ul>\r\n" + 
				"	 *   <li>each class,</li>\r\n" + 
				"	 *   <li>each feature of each class,</li>\r\n" + 
				"	 *   <li>each operation of each class,</li>\r\n" + 
				"	 *   <li>each enum,</li>\r\n" + 
				"	 *   <li>and each data type</li>\r\n" + 
				"	 * </ul>\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	interface Literals {\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '{@link "+_projectName+".impl.Xmod_OperationImpl <em>Xmod Operation</em>}' class.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @see "+_projectName+".impl.Xmod_OperationImpl\r\n" + 
				"		 * @see "+_projectName+".impl."+_projectClass+"PackageImpl#getXmod_Operation()\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EClass XMOD_OPERATION = eINSTANCE.getXmod_Operation();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EAttribute XMOD_OPERATION__NAME = eINSTANCE.getXmod_Operation_Name();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>Object Tag</b></em>' attribute feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EAttribute XMOD_OPERATION__OBJECT_TAG = eINSTANCE.getXmod_Operation_ObjectTag();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>Parameters Tag</b></em>' attribute list feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EAttribute XMOD_OPERATION__PARAMETERS_TAG = eINSTANCE.getXmod_Operation_ParametersTag();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>Return Tag</b></em>' attribute feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EAttribute XMOD_OPERATION__RETURN_TAG = eINSTANCE.getXmod_Operation_ReturnTag();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>Execute</b></em>' operation.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EOperation XMOD_OPERATION___EXECUTE = eINSTANCE.getXmod_Operation__Execute();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '{@link "+_projectName+".impl.Xmod_ActionImpl <em>Xmod Action</em>}' class.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @see "+_projectName+".impl.Xmod_ActionImpl\r\n" + 
				"		 * @see "+_projectName+".impl."+_projectClass+"PackageImpl#getXmod_Action()\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EClass XMOD_ACTION = eINSTANCE.getXmod_Action();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Entry</b></em>' containment reference feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EReference XMOD_ACTION__ON_ENTRY = eINSTANCE.getXmod_Action_OnEntry();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Exit</b></em>' containment reference feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EReference XMOD_ACTION__ON_EXIT = eINSTANCE.getXmod_Action_OnExit();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Do</b></em>' containment reference feature.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EReference XMOD_ACTION__ON_DO = eINSTANCE.getXmod_Action_OnDo();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Entry</b></em>' operation.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EOperation XMOD_ACTION___ON_ENTRY = eINSTANCE.getXmod_Action__OnEntry();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Exit</b></em>' operation.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EOperation XMOD_ACTION___ON_EXIT = eINSTANCE.getXmod_Action__OnExit();\r\n" + 
				"\r\n" + 
				"		/**\r\n" + 
				"		 * The meta object literal for the '<em><b>On Do</b></em>' operation.\r\n" + 
				"		 * <!-- begin-user-doc -->\r\n" + 
				"		 * <!-- end-user-doc -->\r\n" + 
				"		 * @generated\r\n" + 
				"		 */\r\n" + 
				"		EOperation XMOD_ACTION___ON_DO = eINSTANCE.getXmod_Action__OnDo();\r\n" + 
				"\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"Package\r\n" + 
				"";
		        break;
		case "PackageImpl.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+".impl;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EAttribute;\r\n" + 
				"import org.eclipse.emf.ecore.EClass;\r\n" + 
				"import org.eclipse.emf.ecore.EOperation;\r\n" + 
				"import org.eclipse.emf.ecore.EPackage;\r\n" + 
				"import org.eclipse.emf.ecore.EReference;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.impl.EPackageImpl;\r\n" + 
				"\r\n" + 
				"import "+_projectName+"."+_projectClass+"Factory;\r\n" + 
				"import "+_projectName+"."+_projectClass+"Package;\r\n" + 
				"import "+_projectName+".Xmod_Action;\r\n" + 
				"import "+_projectName+".Xmod_Operation;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * An implementation of the model <b>Package</b>.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public class "+_projectClass+"PackageImpl extends EPackageImpl implements "+_projectClass+"Package {\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private EClass xmod_OperationEClass = null;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private EClass xmod_ActionEClass = null;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates an instance of the model <b>Package</b>, registered with\r\n" + 
				"	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package\r\n" + 
				"	 * package URI value.\r\n" + 
				"	 * <p>Note: the correct way to create the package is via the static\r\n" + 
				"	 * factory method {@link #init init()}, which also performs\r\n" + 
				"	 * initialization of the package, or returns the registered package,\r\n" + 
				"	 * if one already exists.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see org.eclipse.emf.ecore.EPackage.Registry\r\n" + 
				"	 * @see "+_projectName+"."+_projectClass+"Package#eNS_URI\r\n" + 
				"	 * @see #init()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private "+_projectClass+"PackageImpl() {\r\n" + 
				"		super(eNS_URI, "+_projectClass+"Factory.eINSTANCE);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private static boolean isInited = false;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.\r\n" + 
				"	 * \r\n" + 
				"	 * <p>This method is used to initialize {@link "+_projectClass+"Package#eINSTANCE} when that field is accessed.\r\n" + 
				"	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @see #eNS_URI\r\n" + 
				"	 * @see #createPackageContents()\r\n" + 
				"	 * @see #initializePackageContents()\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public static "+_projectClass+"Package init() {\r\n" + 
				"		if (isInited)\r\n" + 
				"			return ("+_projectClass+"Package) EPackage.Registry.INSTANCE.getEPackage("+_projectClass+"Package.eNS_URI);\r\n" + 
				"\r\n" + 
				"		// Obtain or create and register package\r\n" + 
				"		"+_projectClass+"PackageImpl the"+_projectClass+"Package = ("+_projectClass+"PackageImpl) (EPackage.Registry.INSTANCE\r\n" + 
				"				.get(eNS_URI) instanceof "+_projectClass+"PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)\r\n" + 
				"						: new "+_projectClass+"PackageImpl());\r\n" + 
				"\r\n" + 
				"		isInited = true;\r\n" + 
				"\r\n" + 
				"		// Create package meta-data objects\r\n" + 
				"		the"+_projectClass+"Package.createPackageContents();\r\n" + 
				"\r\n" + 
				"		// Initialize created meta-data\r\n" + 
				"		the"+_projectClass+"Package.initializePackageContents();\r\n" + 
				"\r\n" + 
				"		// Mark meta-data to indicate it can't be changed\r\n" + 
				"		the"+_projectClass+"Package.freeze();\r\n" + 
				"\r\n" + 
				"		// Update the registry and return the package\r\n" + 
				"		EPackage.Registry.INSTANCE.put("+_projectClass+"Package.eNS_URI, the"+_projectClass+"Package);\r\n" + 
				"		return the"+_projectClass+"Package;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EClass getXmod_Operation() {\r\n" + 
				"		return xmod_OperationEClass;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EAttribute getXmod_Operation_Name() {\r\n" + 
				"		return (EAttribute) xmod_OperationEClass.getEStructuralFeatures().get(0);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EAttribute getXmod_Operation_ObjectTag() {\r\n" + 
				"		return (EAttribute) xmod_OperationEClass.getEStructuralFeatures().get(1);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EAttribute getXmod_Operation_ParametersTag() {\r\n" + 
				"		return (EAttribute) xmod_OperationEClass.getEStructuralFeatures().get(2);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EAttribute getXmod_Operation_ReturnTag() {\r\n" + 
				"		return (EAttribute) xmod_OperationEClass.getEStructuralFeatures().get(3);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EOperation getXmod_Operation__Execute() {\r\n" + 
				"		return xmod_OperationEClass.getEOperations().get(0);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EClass getXmod_Action() {\r\n" + 
				"		return xmod_ActionEClass;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EReference getXmod_Action_OnEntry() {\r\n" + 
				"		return (EReference) xmod_ActionEClass.getEStructuralFeatures().get(0);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EReference getXmod_Action_OnExit() {\r\n" + 
				"		return (EReference) xmod_ActionEClass.getEStructuralFeatures().get(1);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EReference getXmod_Action_OnDo() {\r\n" + 
				"		return (EReference) xmod_ActionEClass.getEStructuralFeatures().get(2);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EOperation getXmod_Action__OnEntry() {\r\n" + 
				"		return xmod_ActionEClass.getEOperations().get(0);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EOperation getXmod_Action__OnExit() {\r\n" + 
				"		return xmod_ActionEClass.getEOperations().get(1);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public EOperation getXmod_Action__OnDo() {\r\n" + 
				"		return xmod_ActionEClass.getEOperations().get(2);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public "+_projectClass+"Factory get"+_projectClass+"Factory() {\r\n" + 
				"		return ("+_projectClass+"Factory) getEFactoryInstance();\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private boolean isCreated = false;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates the meta-model objects for the package.  This method is\r\n" + 
				"	 * guarded to have no affect on any invocation but its first.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public void createPackageContents() {\r\n" + 
				"		if (isCreated)\r\n" + 
				"			return;\r\n" + 
				"		isCreated = true;\r\n" + 
				"\r\n" + 
				"		// Create classes and their features\r\n" + 
				"		xmod_OperationEClass = createEClass(XMOD_OPERATION);\r\n" + 
				"		createEAttribute(xmod_OperationEClass, XMOD_OPERATION__NAME);\r\n" + 
				"		createEAttribute(xmod_OperationEClass, XMOD_OPERATION__OBJECT_TAG);\r\n" + 
				"		createEAttribute(xmod_OperationEClass, XMOD_OPERATION__PARAMETERS_TAG);\r\n" + 
				"		createEAttribute(xmod_OperationEClass, XMOD_OPERATION__RETURN_TAG);\r\n" + 
				"		createEOperation(xmod_OperationEClass, XMOD_OPERATION___EXECUTE);\r\n" + 
				"\r\n" + 
				"		xmod_ActionEClass = createEClass(XMOD_ACTION);\r\n" + 
				"		createEReference(xmod_ActionEClass, XMOD_ACTION__ON_ENTRY);\r\n" + 
				"		createEReference(xmod_ActionEClass, XMOD_ACTION__ON_EXIT);\r\n" + 
				"		createEReference(xmod_ActionEClass, XMOD_ACTION__ON_DO);\r\n" + 
				"		createEOperation(xmod_ActionEClass, XMOD_ACTION___ON_ENTRY);\r\n" + 
				"		createEOperation(xmod_ActionEClass, XMOD_ACTION___ON_EXIT);\r\n" + 
				"		createEOperation(xmod_ActionEClass, XMOD_ACTION___ON_DO);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	private boolean isInitialized = false;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Complete the initialization of the package and its meta-model.  This\r\n" + 
				"	 * method is guarded to have no affect on any invocation but its first.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public void initializePackageContents() {\r\n" + 
				"		if (isInitialized)\r\n" + 
				"			return;\r\n" + 
				"		isInitialized = true;\r\n" + 
				"\r\n" + 
				"		// Initialize package\r\n" + 
				"		setName(eNAME);\r\n" + 
				"		setNsPrefix(eNS_PREFIX);\r\n" + 
				"		setNsURI(eNS_URI);\r\n" + 
				"\r\n" + 
				"		// Create type parameters\r\n" + 
				"\r\n" + 
				"		// Set bounds for type parameters\r\n" + 
				"\r\n" + 
				"		// Add supertypes to classes\r\n" + 
				"\r\n" + 
				"		// Initialize classes, features, and operations; add parameters\r\n" + 
				"		initEClass(xmod_OperationEClass, Xmod_Operation.class, \"Xmod_Operation\", !IS_ABSTRACT, !IS_INTERFACE,\r\n" + 
				"				IS_GENERATED_INSTANCE_CLASS);\r\n" + 
				"		initEAttribute(getXmod_Operation_Name(), ecorePackage.getEString(), \"name\", null, 0, 1, Xmod_Operation.class,\r\n" + 
				"				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);\r\n" + 
				"		initEAttribute(getXmod_Operation_ObjectTag(), ecorePackage.getEString(), \"objectTag\", null, 1, 1,\r\n" + 
				"				Xmod_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,\r\n" + 
				"				!IS_DERIVED, IS_ORDERED);\r\n" + 
				"		initEAttribute(getXmod_Operation_ParametersTag(), ecorePackage.getEString(), \"parametersTag\", null, 0, -1,\r\n" + 
				"				Xmod_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,\r\n" + 
				"				!IS_DERIVED, IS_ORDERED);\r\n" + 
				"		initEAttribute(getXmod_Operation_ReturnTag(), ecorePackage.getEString(), \"returnTag\", null, 0, 1,\r\n" + 
				"				Xmod_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,\r\n" + 
				"				!IS_DERIVED, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		initEOperation(getXmod_Operation__Execute(), null, \"execute\", 0, 1, IS_UNIQUE, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		initEClass(xmod_ActionEClass, Xmod_Action.class, \"Xmod_Action\", !IS_ABSTRACT, !IS_INTERFACE,\r\n" + 
				"				IS_GENERATED_INSTANCE_CLASS);\r\n" + 
				"		initEReference(getXmod_Action_OnEntry(), this.getXmod_Operation(), null, \"onEntry\", null, 0, 1,\r\n" + 
				"				Xmod_Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,\r\n" + 
				"				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);\r\n" + 
				"		initEReference(getXmod_Action_OnExit(), this.getXmod_Operation(), null, \"onExit\", null, 0, 1, Xmod_Action.class,\r\n" + 
				"				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,\r\n" + 
				"				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);\r\n" + 
				"		initEReference(getXmod_Action_OnDo(), this.getXmod_Operation(), null, \"onDo\", null, 0, 1, Xmod_Action.class,\r\n" + 
				"				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,\r\n" + 
				"				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		initEOperation(getXmod_Action__OnEntry(), null, \"onEntry\", 0, 1, IS_UNIQUE, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		initEOperation(getXmod_Action__OnExit(), null, \"onExit\", 0, 1, IS_UNIQUE, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		initEOperation(getXmod_Action__OnDo(), null, \"onDo\", 0, 1, IS_UNIQUE, IS_ORDERED);\r\n" + 
				"\r\n" + 
				"		// Create resource\r\n" + 
				"		createResource(eNS_URI);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"PackageImpl\r\n" + 
				"";
				break;
		case "AdapterFactory.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+".util;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.common.notify.Adapter;\r\n" + 
				"import org.eclipse.emf.common.notify.Notifier;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EObject;\r\n" + 
				"\r\n" + 
				"import "+_projectName+".*;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * The <b>Adapter Factory</b> for the model.\r\n" + 
				" * It provides an adapter <code>createXXX</code> method for each class of the model.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @see "+_projectName+"."+_projectClass+"Package\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public class "+_projectClass+"AdapterFactory extends AdapterFactoryImpl {\r\n" + 
				"	/**\r\n" + 
				"	 * The cached model package.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	protected static "+_projectClass+"Package modelPackage;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates an instance of the adapter factory.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public "+_projectClass+"AdapterFactory() {\r\n" + 
				"		if (modelPackage == null) {\r\n" + 
				"			modelPackage = "+_projectClass+"Package.eINSTANCE;\r\n" + 
				"		}\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns whether this factory is applicable for the type of the object.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return whether this factory is applicable for the type of the object.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public boolean isFactoryForType(Object object) {\r\n" + 
				"		if (object == modelPackage) {\r\n" + 
				"			return true;\r\n" + 
				"		}\r\n" + 
				"		if (object instanceof EObject) {\r\n" + 
				"			return ((EObject) object).eClass().getEPackage() == modelPackage;\r\n" + 
				"		}\r\n" + 
				"		return false;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * The switch that delegates to the <code>createXXX</code> methods.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	protected "+_projectClass+"Switch<Adapter> modelSwitch = new "+_projectClass+"Switch<Adapter>() {\r\n" + 
				"		@Override\r\n" + 
				"		public Adapter caseXmod_Operation(Xmod_Operation object) {\r\n" + 
				"			return createXmod_OperationAdapter();\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"		@Override\r\n" + 
				"		public Adapter caseXmod_Action(Xmod_Action object) {\r\n" + 
				"			return createXmod_ActionAdapter();\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"		@Override\r\n" + 
				"		public Adapter defaultCase(EObject object) {\r\n" + 
				"			return createEObjectAdapter();\r\n" + 
				"		}\r\n" + 
				"	};\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates an adapter for the <code>target</code>.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param target the object to adapt.\r\n" + 
				"	 * @return the adapter for the <code>target</code>.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public Adapter createAdapter(Notifier target) {\r\n" + 
				"		return modelSwitch.doSwitch((EObject) target);\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates a new adapter for an object of class '{@link "+_projectName+".Xmod_Operation <em>Xmod Operation</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This default implementation returns null so that we can easily ignore cases;\r\n" + 
				"	 * it's useful to ignore a case when inheritance will catch all the cases anyway.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the new adapter.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Operation\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public Adapter createXmod_OperationAdapter() {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates a new adapter for an object of class '{@link "+_projectName+".Xmod_Action <em>Xmod Action</em>}'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This default implementation returns null so that we can easily ignore cases;\r\n" + 
				"	 * it's useful to ignore a case when inheritance will catch all the cases anyway.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the new adapter.\r\n" + 
				"	 * @see "+_projectName+".Xmod_Action\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public Adapter createXmod_ActionAdapter() {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates a new adapter for the default case.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This default implementation returns null.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the new adapter.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public Adapter createEObjectAdapter() {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"AdapterFactory\r\n" + 
				"";
				break;
		case "Switch.java": returnValue = "/**\r\n" + 
				" */\r\n" + 
				"package "+_projectName+".util;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.EObject;\r\n" + 
				"import org.eclipse.emf.ecore.EPackage;\r\n" + 
				"\r\n" + 
				"import org.eclipse.emf.ecore.util.Switch;\r\n" + 
				"\r\n" + 
				"import "+_projectName+".*;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * <!-- begin-user-doc -->\r\n" + 
				" * The <b>Switch</b> for the model's inheritance hierarchy.\r\n" + 
				" * It supports the call {@link #doSwitch(EObject) doSwitch(object)}\r\n" + 
				" * to invoke the <code>caseXXX</code> method for each class of the model,\r\n" + 
				" * starting with the actual class of the object\r\n" + 
				" * and proceeding up the inheritance hierarchy\r\n" + 
				" * until a non-null result is returned,\r\n" + 
				" * which is the result of the switch.\r\n" + 
				" * <!-- end-user-doc -->\r\n" + 
				" * @see "+_projectName+"."+_projectClass+"Package\r\n" + 
				" * @generated\r\n" + 
				" */\r\n" + 
				"public class "+_projectClass+"Switch<T> extends Switch<T> {\r\n" + 
				"	/**\r\n" + 
				"	 * The cached model package\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	protected static "+_projectClass+"Package modelPackage;\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Creates an instance of the switch.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public "+_projectClass+"Switch() {\r\n" + 
				"		if (modelPackage == null) {\r\n" + 
				"			modelPackage = "+_projectClass+"Package.eINSTANCE;\r\n" + 
				"		}\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Checks whether this is a switch for the given package.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param ePackage the package in question.\r\n" + 
				"	 * @return whether this is a switch for the given package.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	protected boolean isSwitchFor(EPackage ePackage) {\r\n" + 
				"		return ePackage == modelPackage;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @return the first non-null result returned by a <code>caseXXX</code> call.\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	protected T doSwitch(int classifierID, EObject theEObject) {\r\n" + 
				"		switch (classifierID) {\r\n" + 
				"		case "+_projectClass+"Package.XMOD_OPERATION: {\r\n" + 
				"			Xmod_Operation xmod_Operation = (Xmod_Operation) theEObject;\r\n" + 
				"			T result = caseXmod_Operation(xmod_Operation);\r\n" + 
				"			if (result == null)\r\n" + 
				"				result = defaultCase(theEObject);\r\n" + 
				"			return result;\r\n" + 
				"		}\r\n" + 
				"		case "+_projectClass+"Package.XMOD_ACTION: {\r\n" + 
				"			Xmod_Action xmod_Action = (Xmod_Action) theEObject;\r\n" + 
				"			T result = caseXmod_Action(xmod_Action);\r\n" + 
				"			if (result == null)\r\n" + 
				"				result = defaultCase(theEObject);\r\n" + 
				"			return result;\r\n" + 
				"		}\r\n" + 
				"		default:\r\n" + 
				"			return defaultCase(theEObject);\r\n" + 
				"		}\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the result of interpreting the object as an instance of '<em>Xmod Operation</em>'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This implementation returns null;\r\n" + 
				"	 * returning a non-null result will terminate the switch.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param object the target of the switch.\r\n" + 
				"	 * @return the result of interpreting the object as an instance of '<em>Xmod Operation</em>'.\r\n" + 
				"	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public T caseXmod_Operation(Xmod_Operation object) {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the result of interpreting the object as an instance of '<em>Xmod Action</em>'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This implementation returns null;\r\n" + 
				"	 * returning a non-null result will terminate the switch.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param object the target of the switch.\r\n" + 
				"	 * @return the result of interpreting the object as an instance of '<em>Xmod Action</em>'.\r\n" + 
				"	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	public T caseXmod_Action(Xmod_Action object) {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"	/**\r\n" + 
				"	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.\r\n" + 
				"	 * <!-- begin-user-doc -->\r\n" + 
				"	 * This implementation returns null;\r\n" + 
				"	 * returning a non-null result will terminate the switch, but this is the last case anyway.\r\n" + 
				"	 * <!-- end-user-doc -->\r\n" + 
				"	 * @param object the target of the switch.\r\n" + 
				"	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.\r\n" + 
				"	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)\r\n" + 
				"	 * @generated\r\n" + 
				"	 */\r\n" + 
				"	@Override\r\n" + 
				"	public T defaultCase(EObject object) {\r\n" + 
				"		return null;\r\n" + 
				"	}\r\n" + 
				"\r\n" + 
				"} //"+_projectClass+"Switch\r\n" + 
				"";
				break;
		case "XmodUtil.java": returnValue = "package "+_projectName+".util;\r\n" + 
    			"\r\n" + 
    			"import java.beans.XMLDecoder;\r\n" + 
    			"import java.beans.XMLEncoder;\r\n" + 
    			"import java.io.BufferedInputStream;\r\n" + 
    			"import java.io.BufferedOutputStream;\r\n" + 
    			"import java.io.File;\r\n" + 
    			"import java.io.FileInputStream;\r\n" + 
    			"import java.io.FileNotFoundException;\r\n" + 
    			"import java.io.FileOutputStream;\r\n" + 
    			"import java.util.HashMap;\r\n" + 
    			"\r\n" + 
    			"import org.eclipse.emf.common.util.URI;\r\n" + 
    			"import org.eclipse.emf.ecore.EObject;\r\n" + 
    			"import org.eclipse.emf.ecore.EPackage;\r\n" + 
    			"import org.eclipse.emf.ecore.resource.Resource;\r\n" + 
    			"import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;\r\n" + 
    			"import org.eclipse.emf.ecore.xmi.XMLResource;\r\n" + 
    			"import org.eclipse.emf.ecore.xmi.XMLResource.XMLMap;\r\n" + 
    			"import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;\r\n" + 
    			"import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;\r\n" + 
    			"\r\n" + 
    			"/**\r\n" + 
    			" * <b> "+_projectClass+"XmodUtil is a class that provides tools to manipulate the model and the map. </b>\r\n" + 
    			" * \r\n" + 
    			" * @author LeaBrunschwig\r\n" + 
    			" */" +
    			"public class "+_projectClass+"XmodUtil {\r\n" + 
    			"	\r\n" + 
    			"	/**\r\n" + 
    			"	 * The map containing all the tags.\r\n" + 
    			"	 * \r\n" + 
    			"	 * @see #getMap()\r\n" + 
    			"	 * @see #setMap(HashMap)\r\n" + 
    			"	 * @see #loadMap(String)\r\n" + 
    			"	 * @see #saveMap(String)\r\n" + 
    			"	 */" +
    			"	private static HashMap<String,Object> hm;\r\n" + 
    			"	\r\n" + 
    			"	/**\r\n" + 
    			"	 * Returns the value of the map.\r\n" + 
    			"	 * <p>The singleton design pattern is used.</p>\r\n" + 
    			"	 * @return the map hm.\r\n" + 
    			"	 * @see #hm\r\n" + 
    			"	 */" +
    			"	public static HashMap<String,Object> getMap() {\r\n" + 
    			"		if (hm == null) {\r\n" + 
    			"			hm = new HashMap<String,Object>();\r\n" + 
    			"		}\r\n" + 
    			"		return hm;\r\n" + 
    			"	}\r\n" + 
    			"\r\n" + 
    			"	/**\r\n" + 
    			"	 * Updates the map hm.\r\n" + 
    			"	 * @param hm\r\n" + 
    			"	 * 			Map that is going to be set to the attribute hm.\r\n" + 
    			"	 * @see #hm\r\n" + 
    			"	 */" +
    			"	public static void setMap(HashMap<String,Object> hm) {\r\n" + 
    			"		"+_projectClass+"XmodUtil.hm = hm;\r\n" + 
    			"	}\r\n" + 
    			"\r\n" + 
    			"	/**\r\n" + 
    			"	 * Loads a map from a file.\r\n" + 
    			"	 * <p>The map is deserialized.</p>\r\n" + 
    			"	 * @param fileName\r\n" + 
    			"	 * 			Name of the file containing a map.\r\n" + 
    			"	 * @return a map containing the map from the given file.\r\n" + 
    			"	 */" +
    			"	@SuppressWarnings(\"unchecked\")\r\n" + 
    			"	public static java.util.Map<String, Object> loadMap(String fileName) {\r\n" + 
    			"\r\n" + 
    			"		/* Init */\r\n" + 
    			"		File xmlFile;\r\n" + 
    			"		XMLDecoder decoder = null;\r\n" + 
    			"		HashMap<String, Object> hm = new HashMap<String, Object>();\r\n" + 
    			"\r\n" + 
    			"		/* Body */\r\n" + 
    			"		try {\r\n" + 
    			"			xmlFile = new File(fileName);\r\n" + 
    			"			if (xmlFile.exists()) { // if an xml file with the name of the map does exist, we deserialize it.\r\n" + 
    			"				decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(xmlFile)));\r\n" + 
    			"				hm = (HashMap<String, Object>) decoder.readObject();\r\n" + 
    			"				decoder.close();\r\n" + 
    			"			}\r\n" + 
    			"\r\n" + 
    			"		} catch (FileNotFoundException e1) {\r\n" + 
    			"			e1.printStackTrace();\r\n" + 
    			"		}\r\n" + 
    			"\r\n" + 
    			"		return hm;\r\n" + 
    			"	}\r\n" + 
    			"	\r\n" + 
    			"	/**\r\n" + 
    			"	 * Saves a map in a file.\r\n" + 
    			"	 * <p>The map is serialized.</p>\r\n" + 
    			"	 * @param fileName\r\n" + 
    			"	 * 			Name of the file where the map is going to be saved.\r\n" + 
    			"	 */" +
    			"	public static void saveMap(String fileName) {\r\n" + 
    			"\r\n" + 
    			"		/* Init */\r\n" + 
    			"		File xmlFile;\r\n" + 
    			"		XMLEncoder encoder = null;\r\n" + 
    			"\r\n" + 
    			"		/* Body */\r\n" + 
    			"		try {\r\n" + 
    			"			xmlFile = new File(fileName);\r\n" + 
    			"			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(xmlFile)));\r\n" + 
    			"			encoder.writeObject(getMap());\r\n" + 
    			"			encoder.flush();\r\n" + 
    			"		} catch (FileNotFoundException e) {\r\n" + 
    			"			e.printStackTrace();\r\n" + 
    			"		} finally {\r\n" + 
    			"			// closing of the encoder\r\n" + 
    			"			encoder.close();\r\n" + 
    			"		}\r\n" + 
    			"	}\r\n" + 
    			"	\r\n" + 
    			"\r\n" + 
    			"	/**\r\n" + 
    			"	 * Saves a given model at a given URI.\r\n" + 
    			"	 * @param uri\r\n" + 
    			"	 * 			The URI where the model will be saved.\r\n" + 
    			"	 * @param root\r\n" + 
    			"	 * 			The root of the model that is going to be saved.\r\n" + 
    			"	 */" +
    			"	public static void saveModel(String uri, EObject root) {\r\n" + 
    			"		Resource resource = null;\r\n" + 
    			"		try {\r\n" + 
    			"			URI uriUri = URI.createURI(uri);\r\n" + 
    			"			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(\"xmi\", new XMIResourceFactoryImpl());\r\n" + 
    			"			resource = (new ResourceSetImpl()).createResource(uriUri);\r\n" + 
    			"			resource.getContents().add(root);\r\n" + 
    			"			resource.save(null); \r\n" + 
    			"		} catch (Exception e) { \r\n" + 
    			"			System.err.println(\"Error in saving the model: \"+e);\r\n" + 
    			"			e.printStackTrace(); \r\n" + 
    			"		} \r\n" + 
    			"	}\r\n" + 
    			"\r\n" + 
    			"	/**\r\n" + 
    			"	 * Load a model from a given URI.\r\n" + 
    			"	 * @param uri\r\n" + 
    			"	 * 			The URI where the model is.\r\n" + 
    			"	 * @param pack\r\n" + 
    			"	 * 			an instance of EPackage\r\n" + 
    			"	 * @return a model\r\n" + 
    			"	 */" +
    			"	public static Resource loadModel(String uri, EPackage pack) {\r\n" + 
    			"		Resource resource = null;\r\n" + 
    			"		try {\r\n" + 
    			"			URI uriUri = URI.createURI(uri);\r\n" + 
    			"			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(\"xmi\", new XMIResourceFactoryImpl());\r\n" + 
    			"			resource = (new ResourceSetImpl()).createResource(uriUri);\r\n" + 
    			"			XMLResource.XMLMap xmlMap = new XMLMapImpl();\r\n" + 
    			"			xmlMap.setNoNamespacePackage(pack);\r\n" + 
    			"			java.util.Map<String, XMLMap> options = new java.util.HashMap<String, XMLMap>();\r\n" + 
    			"			options.put(XMLResource.OPTION_XML_MAP, xmlMap);\r\n" + 
    			"			resource.load(options);    \r\n" + 
    			"		}\r\n" + 
    			"		catch(Exception e) {\r\n" + 
    			"			System.err.println(\"Error while loading the model: \"+e);\r\n" + 
    			"			e.printStackTrace();\r\n" + 
    			"		}\r\n" + 
    			"		return resource;\r\n" + 
    			"	}\r\n" + 
    			"	\r\n" + 
    			"}" +
    			"";
				break;
        default: returnValue = null;
                 break;
		}
		
		return returnValue;
	}
	
}
