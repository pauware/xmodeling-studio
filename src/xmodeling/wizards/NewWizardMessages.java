package xmodeling.wizards;

import org.eclipse.osgi.util.NLS;

public class NewWizardMessages extends NLS {
	private static final String BUNDLE_NAME = "xmodeling.wizards.messages"; //$NON-NLS-1$
	public static String XmodelingProject_XmodelingProjectNewWizard_description;
	public static String XmodelingProject_XmodelingProjectNewWizard_page_name;
	public static String XmodelingProject_XmodelingProjectNewWizard_title;
	public static String XmodelingProject_XmodelingProjectNewWizard_wizard_name;
	public static String XmodelingProject_XmodelingProjectNewWizardFromExistingEcore_wizard_name;
	public static String XmodelingProject_XmodelingProjectNewWizardFromExistingEcore_title;
	public static String XmodelingProject_XmodelingProjectNewWizardFromExistingEcore_page_name;
	public static String XmodelingProject_XmodelingProjectNewWizardFromExistingEcore_description;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, NewWizardMessages.class);
	}

	private NewWizardMessages() {
	}
}
